<?php include('header.php');
	
	if(!$_SESSION['r_admin_name']){
		header('Location: index.php');
	}

	if (isset($_POST['n_submit']) && $_POST['a_pass']== $_POST['n_pass'] && !empty($_POST['a_access'])) {

		$admin_insert_obj = new Add_Admin;

		$admin_insert_obj->admin_insert($_POST);

	}
	
?>
		<!-- ===============###########=== Content Part Start ======################=============== -->	
			<div class="col-sm-8">
			
			<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
			
			  <h1 class="text-center">Add New Admin</h1>
			   <div class="form-group">
				<label for="password">Admin Accsee</label>
				<select name="n_accsee" class="form-control">
					<option value="admin">Choose Admin Access</option>
					<option value="admin">Admin</option>
					<option value="editor">Editor</option>
					<option value="operator">Oparator</option>
					<option value="admin">Admin</option>
				</select>
			  </div>
			  <div class="form-group">
				<label for="name">Admin Name</label>
				<input name="a_name" type="text" class="form-control"  placeholder="Add Admin Name">
			  </div>
			   <div class="form-group">
				<label for="email">Admin Email</label>
				<input name="a_email" type="email" class="form-control"  placeholder="Add Admin Email">
			  </div>
			   <div class="form-group">
				<label for="password">Admin Password</label>
				<input name="a_pass" type="password" class="form-control"  placeholder="Add Admin Password">
			  </div>
			  <div class="form-group">
				<label for="password">Confirm Password</label>
				<input name="n_pass" type="password" class="form-control"  placeholder="Confirm Password">
			  </div>

			  <button name="n_submit" type="submit" class="btn btn-default">Submit</button>
			</form>
			</div>
		
		<!-- ===============###########=== Content Part close ======################=============== -->	
		
<?php include('footer.php'); ?>
