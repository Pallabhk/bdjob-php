<?php
	include('header.php');
	//require('header.php');

?>
<style>
	#all_product{
		padding-top:10px;
		padding-bottom:25px;
	}
	#all_product a{
		text-decoration:none;
	}
	#all_product img{
		height: 200px;
	}
	#all_product h3{
		font-size:16px;
		color: #af4726;
		font-weight: bold;
	}
	#all_product p{
		border-top:1px solid gray;
		padding-top: 10px;
		font-size: 1.2em;
	}
</style>
<!-- ============= Body Content Part ================ -->
<!-- ============= Product NavBar ================ -->
<?php include('productNav.php');?>
<!-- ============= Body Content Part ================ -->
<section id="all_product">
	<div class="container">
		<div class="row" id="product_view">
<?php
	require_once('class_lib/user_product_view_class.php');
	$product_obj= new userPRODUCTview;
	$product=$product_obj->limit_product_view();
	
	if($product->num_rows > 0){
		while($p_data=$product->fetch_assoc()){
			///print_r($p_data);
			
			$product_id=$p_data['sl_id'];
			$product_img=$p_data['product_image'];
			$product_name=$p_data['product_name'];
			$product_price=$p_data['product_price'];
			$product_code=$p_data['product_code'];
		?>
			<div class="col-sm-3 col-xs-6">
				<a href="product_detail.php?p_code=<?php echo $product_code; ?>" class="thumbnail">
				  <img src="<?php echo $product_img; ?>" alt="<?php echo $product_name; ?>">
				  <div class="caption text-center">
					<h3><?php echo $product_name; ?></h3>
					<p><?php echo $product_price; ?>/-tk</p>
					<p><?php echo $product_code; ?></p>
				  </div>
				</a>
			</div>		
		<?php		
		}

	}

?>
			<div class="col-xs-12 text-center" id="remove_btn">
				<button type="button" id="loadmore" data-id="<?php echo$product_id; ?>" class="btn btn-primary">Show More Product</button>
			</div>
		</div>
	</div>
</section>

<!-- ============ Footer Part ============= -->
<?php
	require'footer.php';
?>
<script>
	$(document).ready(function(){
		//$('#loadmore').click(function(){
			$(document).on('click','#loadmore',function(){
			var last_data= $(this).data('id');
			$(this).html('Loading..');
			$.ajax({
				url:'loadmore.php',
				method:'POST',
				data:{'last_data':last_data},
				success:function(data){
					//alert(data);
					if(data!=""){
						$('#remove_btn').remove();
						$('#product_view').append(data);
						//alert(data);
					}else{
						$('#loadmore').html('No More Data');
					}
					
				}
			});
			
		});
	
	})
</script>