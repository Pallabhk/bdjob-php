<?php
	
	require_once('db_connection_class.php');
	
	class userPRODUCTview extends DB_CONNECT{
		
		########################################
		########## All Product View ############
		public function all_product_view(){
			
			$db_connt=$this->connect;
			$sql_view="SELECT * FROM all_products";
			$result=$db_connt->query($sql_view);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// all_product_view method
		
		########################################
		########## Limit Product View ############
		public function limit_product_view(){
			
			$db_connt=$this->connect;
			$sql_view="SELECT * FROM all_products LIMIT 4";
			$result=$db_connt->query($sql_view);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// limit_product_view method
		
		########################################
		########## Limit wise Product View ############
		public function limit_wise_product_view($data){
			
			$db_connt=$this->connect;
			$sql_view="SELECT * FROM all_products LIMIT $data, 4";
			$result=$db_connt->query($sql_view);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// limit_wise_product_view method
		
		##############################################
		########## Category wise Product View ############
		
		public function category_product_view($data1, $data2){
			
			$db_connt=$this->connect;
			$sql_view_category="SELECT * FROM all_products WHERE product_main_categ='$data1' && product_sub_categ='$data2'";
			$result=$db_connt->query($sql_view_category);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// category_product_view method
		
		##############################################
		########## Code wise Product View ############
		
		public function code_product_view($data){
			
			$db_connt=$this->connect;
			$sql_view_code="SELECT * FROM all_products WHERE product_code='$data'";
			$result=$db_connt->query($sql_view_code);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// code_product_view method
		
		
		
	}// userPRODUCTview class
	
	
	
	
	
	
	
	
	
	
	
	
	
?>