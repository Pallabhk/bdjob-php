<?php 
ob_start();
include('header.php');
if(!$_SESSION['action']){
	header('Location: index.php');
}
########################
require_once('../class_lib/add_product_class.php');
	$last_id_obj= new Add_Product;
	$last_id=$last_id_obj->last_id();
?>

		<!-- ===============###########=== Content Part Start ======################=============== -->	
			<div class="col-sm-8">
<?php
if(isset($_POST['prod_submit']) && !empty($_POST['prod_categ'])){
	require_once('../class_lib/add_product_class.php');
	$prod_insert_obj= new Add_Product;
	$prod_insert_obj->product_insert($_POST);
}

?>

				<!-- ======================== Add Product =============== -->
				<h3 class="alert alert-success text-center">Add Product</h3>
				<input id="product_serial" class="sr-only" value="<?php echo $last_id; ?>">
<?php
	require_once('../class_lib/main_category_class.php');
	$main_categ_obj= new Main_Category;
	/////////// for Sub Category form
	$main_categ_data=$main_categ_obj->main_categ_view();
?>
				<form method="post" enctype="multipart/form-data" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				  <div class="form-group">
					<label for="prod_categ">Choose Product Category</label>
					<select name="prod_categ" class="form-control" id="prod_categ">
						<option value="">Choose Product Category</option>
				<?php
					if($main_categ_data->num_rows>0){
						while($main_categ_list=$main_categ_data->fetch_assoc()){
							
							$main_categ_name=$main_categ_list['main_categ_name'];
							$main_categ_value=$main_categ_list['main_categ_folder'];
							
							echo '<option disabled="disabled" style="font-weight:700; background-color:skyblue; border-bottom:1px solid black;">'.$main_categ_name.'</option>';
							
							########################## Sub Category view	
								require_once('../class_lib/sub_category_class.php');
								$sub_categ_obj= new Sub_Category;
								$sub_category_table=$sub_categ_obj->sub_categ_view_main($main_categ_value);
								
								if($sub_category_table->num_rows > 0){
									while($sub_categ_list=$sub_category_table->fetch_assoc()){
										$sub_categ_name=$sub_categ_list['sub_categ_name'];
										$sub_categ_value=$sub_categ_list['sub_categ_folder'];
										///print_r($sub_categ_list);
										
										echo '<option value="'.$main_categ_value.'#'.$sub_categ_value.'">'.$sub_categ_name.' of '.$main_categ_name.'</option>';
										
									}////// While Loop							
								}
								
						}
					}else{
						echo '<option value="">There have no Main Category</option>';
					}
				
				?>
					</select>
				  </div>
				  <div class="form-group">
					<label for="prod_code">Product Code</label>
					<input name="prod_code" type="text" class="form-control" id="prod_code" value="" placeholder="Product Code">
				  </div>
				  <div class="form-group">
					<label for="prod_name">Product Name</label>
					<input name="prod_name" type="text" class="form-control" id="prod_name"  placeholder="Product Name">
				  </div>
				  <div class="form-group">
					<label for="prod_price">Product Price</label>
					<input name="prod_price" type="text" class="form-control" id="prod_price"  placeholder="Product Price">
				  </div>
				  <div class="form-group">
					<label for="prod_desc">Product Description</label>
					<textarea name="prod_desc" type="text" rows="3" class="form-control" id="prod_desc"  placeholder="Product Description"></textarea>
				  </div>
				  <div class="form-group">
					<label for="prod_img">Product Image</label>
					<input type="file" name="prod_img" id="prod_img">
				  </div>
				  <div class="form-group">
				  <button name="prod_submit" type="submit" class="btn btn-primary">Product Submit</button>
				  </div>
				</form>
				<br>
				
				
			</div><!-- Content div -->
		
		<!-- ===============###########=== Content Part close ======################=============== -->	


<?php 
include('footer.php'); 
ob_end_flush();
ob_end_clean();
?>
<script>
/*
	$(document).ready(function(){
		$('#prod_categ').change(function(){
			var main_categ_name= $('#prod_categ').val();
			alert(main_categ_name);
		});
	})
*/
</script>
<script>
$('#prod_code').hide();
$('#prod_categ').change(function(){
var str = $('#prod_categ').val(); 
var last_id = $('#product_serial').val(); 
var today = new Date(); var dd = today.getDate(); var mm = today.getMonth()+1; var yyyy = today.getFullYear(); if(dd<10){dd = '0'+dd;} if(mm<10){mm = '0'+mm;} today = mm + yyyy + dd;var part = str.split("#"); var x,i; var text=''; if(part[0].search("_")>0){ var main= part[0].split("_"); for(x=0; x < main.length; x++){text += main[x][0];}}else{text+=part[0][0];} if(part[1].search('-')>0){ var sub= part[1].split("-"); for(x=0; x < sub.length; x++){ if(sub[x].search('_')>0){ var subsub= sub[x].split("_"); for(i=0; i < subsub.length; i++){text += subsub[i][0];}}else{text += sub[x][0];}}}else {if(part[1].search('_')>0){ var sub= part[1].split("_"); for(x=0; x < sub.length; x++){ text += sub[x][0]; }}else{text+=part[1][0];}}

//////// Make the final Product Code ///////
var itemcode =text+today+last_id;
$('#prod_code').show();
$('#prod_code').val(itemcode.toUpperCase());
});
</script>	