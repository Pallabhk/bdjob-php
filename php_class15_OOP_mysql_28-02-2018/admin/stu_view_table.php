<?php
	require_once('../functions.php');
		$student=student_data_table();
		$student_row=$student->num_rows;
		echo '<pre>';
		//print_r($student);
		//print_r(mysqli_fetch_array($student));
		//print_r(mysqli_fetch_assoc($student));
		echo '</pre>';
		
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">  

  
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Sl</th>
        <th>Student Name</th>
        <th>Father Name</th>
        <th>Mother Name</th>
        <th>DoB</th>
        <th>Gender</th>
        <th>Phone</th>
        <th>Email</th>
        <th>NID</th>
        <th>Marital Status</th>
        <th>Blood</th>
        <th>Divition</th>
        <th>Courses</th>
        <th>FB Url</th>
        <th>Father Occupation</th>
        <th>Present Addrs</th>
        <th>Permanent Addrs</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
<?php
	if($student_row >0){
		$x=1;
		while($stu_detail=mysqli_fetch_assoc($student)){
			?>
			
	  <tr>
        <td><?php echo $x++; ?></td>
        <td><?php echo $stu_detail['student_name']; ?></td>
        <td><?php echo $stu_detail['stu_father_name']; ?></td>
        <td><?php echo $stu_detail['stu_mother_name']; ?></td>
        <td><?php echo $stu_detail['stu_dob']; ?></td>
        <td><?php echo $stu_detail['stu_gender']; ?></td>
        <td><?php echo $stu_detail['stu_phone']; ?></td>
        <td><?php echo $stu_detail['stu_email']; ?></td>
        <td><?php echo $stu_detail['stu_nid']; ?></td>
        <td><?php echo $stu_detail['stu_mstatus']; ?></td>
        <td><?php echo $stu_detail['stu_bgroup']; ?></td>
        <td><?php echo $stu_detail['stu_div']; ?></td>
        <td><?php echo $stu_detail['stu_course']; ?></td>
        <td><?php echo $stu_detail['stu_fburl']; ?></td>
        <td><?php echo $stu_detail['stu_foccu']; ?></td>
        <td><?php echo $stu_detail['stu_paddr']; ?></td>
        <td><?php echo $stu_detail['stu_prmaddr']; ?></td>
        <td><a href="stu_info_update.php?stu_edit=<?php echo $stu_detail['sl_id']; ?>" role="button" class="btn btn-success">Edit</a> &nbsp; <a href="index.php?stu_del=<?php echo $stu_detail['sl_id']; ?>" role="button" class="btn btn-danger">Delete</a></td>
      </tr>		
			<?php
		}
	}
	else{
	?>
	  <tr>
        <td colspan="17" class="text-center">There have no Data</td>      
	  </tr>
	
	<?php
	}
	 ####### delete function ##########
	if(isset($_GET['stu_del'])){
		require_once('../functions.php');
		student_data_delete($_GET['stu_del']);
	}
?>

	  
     
    </tbody>
  </table>
  </div>
</div>

</body>
</html>
