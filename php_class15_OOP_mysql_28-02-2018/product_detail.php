<?php
	include('header.php');
	//require('header.php');
//echo $_GET['p_code'];
	###################Code wise Prodcut view ############
	require_once('class_lib/user_product_view_class.php');
	$product_obj= new userPRODUCTview;
	$product=$product_obj->code_product_view($_GET['p_code']);
	$p_data=$product->fetch_assoc();
	
			$product_img=$p_data['product_image'];
			$product_name=$p_data['product_name'];
			$product_price=$p_data['product_price'];
			$product_code=$p_data['product_code'];
			$product_description=$p_data['product_desc'];
	///print_r($p_data);
?>
<style>
#product_dtl{
	margin-bottom:25px;
}
#product_dtl .tab-content{
	padding: 25px 5px;
    min-height: 250px;
}
@media (max-width:450px){
	#product_dtl{
		padding-left:2px; 
		padding-right:2px;
	}
	#product_dtl ul li a{
		padding:10px 3px;
	}
}	
	
</style>
<!-- ============= Body Content Part ================ -->
<!-- ============= Product NavBar ================ -->
<?php include('productNav.php');?>
<!-- ============= Body Content Part ================ -->
<section id="product_detail">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="thumbnail">
				  <img src="<?php echo $product_img; ?>" alt="<?php echo $product_name; ?>" style="height:400px;">
				</div>
			</div>
			<div id="product_dtl" class="col-sm-6 col-xs-12">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#product_details" aria-controls="product_details" role="tab" data-toggle="tab">Product Detials</a></li>
					<li role="presentation"><a href="#product_description" aria-controls="product_description" role="tab" data-toggle="tab">Product Description</a></li>
					<li role="presentation"><a href="#product_review" aria-controls="product_review" role="tab" data-toggle="tab">Prodcut Review</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="product_details">
						<p><b>Product Code: </b><?php echo $product_code; ?></p>
						<p><b>Product Name: </b><?php echo $product_name; ?></p>
						<p><b>Product price: </b><?php echo $product_price; ?></p>
						<p><b>Product quantity: </b><input type="number" name="p_quantity" value="1" max="10" min="1"></p>

					</div>
					<div role="tabpanel" class="tab-pane" id="product_description">
						<p><b>Product Description: </b><?php echo $product_description; ?></p>
					</div>
					<div role="tabpanel" class="tab-pane" id="product_review">Product Review:3.5
					<?php
						//echo ceil(3.9);

						//echo round(3.4);
						$review =round(0);//Database

						for($i= 1; $i<=5; $i++){
							if ($i<=$review) {
								echo "<Span class='glyphicon glyphicon-star' style='color:yellow; text-shadow:0px 2px 3px red; font-size:16px; padding:0 2px; aria-hidden:'true'></span>";
						}else{
						echo "<Span class='glyphicon glyphicon-star' style='color:black; text-shadow:0px 2px 3px green; font-size:16px; padding:0 2px; aria-hidden:'true'></span>";
						}
					}
							
						
					?>
					</div>
				  </div>
				  
				  
				 
				<div class="btn-group btn-group-justified" role="group" aria-label="...">
				  <div class="btn-group" role="group">
					<button type="button" class="btn btn-warning">Wish List</button>
				  </div>
				  <div class="btn-group" role="group">
					<button type="button" data-toggle="modal" data-target="#userlogin" class="btn btn-primary">Add to Cart</button>
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ============ Footer Part ============= -->
<?php
	require'footer.php';
?>