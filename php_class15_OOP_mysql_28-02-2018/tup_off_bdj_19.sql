-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2018 at 01:16 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tup_off_bdj_19`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_access`
--

CREATE TABLE `admin_access` (
  `sl_id` int(2) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_pass` varchar(100) NOT NULL,
  `admin_action` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_access`
--

INSERT INTO `admin_access` (`sl_id`, `admin_name`, `admin_email`, `admin_pass`, `admin_action`) VALUES
(1, 'Md. Sharif Ullah Sarkar', 'manikbd.888@gmail.com', 'manikbd123456', 'root_admin'),
(2, 'Manik Sarkar', 'manikbd.fx@gmail.com', '123456', 'admin'),
(3, 'Sharmin Akter', 'sharmindpi@gmail.com', '123456', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `all_products`
--

CREATE TABLE `all_products` (
  `sl_id` int(6) NOT NULL,
  `product_code` varchar(30) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` float NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_main_categ` varchar(50) NOT NULL,
  `product_sub_categ` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `all_products`
--

INSERT INTO `all_products` (`sl_id`, `product_code`, `product_name`, `product_price`, `product_desc`, `product_image`, `product_main_categ`, `product_sub_categ`) VALUES
(2, 'M20180214-2', 'Red Checked Shirt', 1500, 'Red Checked Shirt Red Checked Shirt Red Checked Shirt Red Checked Shirt ', 'images/all_products/men/formal_shirt/1518609515t_shirt2.jpg', 'men', 'formal_shirt'),
(3, 'M20180214-3', 'Pink Checked Shirt', 2000, 'Pink Checked Shirt Pink Checked Shirt Pink Checked Shirt Pink Checked Shirt ', 'images/all_products/men/formal_shirt/M20180214-31518609679t_shirt1.jpg', 'men', 'formal_shirt'),
(4, '20180214-4', 'Black  Shirt', 1200, 'Black  Shirt Black  Shirt Black  Shirt Black  Shirt Black  Shirt Black  Shirt ', 'images/all_products///20180214-41518609877t_shirt3.jpg', '', ''),
(5, 'M20180214-5', 'Black  Shirt', 1200, 'Black  Shirt Black  Shirt Black  Shirt Black  Shirt Black  Shirt ', 'images/all_products/men/formal_shirt/M20180214-51518610096t_shirt3.jpg', 'men', 'formal_shirt'),
(6, 'M20180219-6', 'Yellow Casual Pant', 1500, 'Yellow Casual Pant Yellow Casual Pant Yellow Casual Pant Yellow Casual Pant Yellow Casual Pant Yellow Casual Pant ', 'images/all_products/men/formal_pant/M20180219-61519029472Pant03.jpg', 'men', 'formal_pant'),
(7, 'M20180219-7', 'Blue Jeans Pant', 1200, 'Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant Blue Jeans Pant ', 'images/all_products/men/formal_pant/M20180219-71519029683Pant02.jpeg', 'men', 'formal_pant'),
(8, 'MMTS022018268', 'Pink Rounded Neck T-Shirt', 150, 'Pink Rounded Neck T-Shirt Pink Rounded Neck T-Shirt Pink Rounded Neck T-Shirt Pink Rounded Neck T-Shirt ', 'images/all_products/men/t-shirt/MMTS0220182681519641926t_shirt1.jpg', 'men', 't-shirt'),
(9, 'MTS022018269', 'Black Spider T-shirt', 100, 'Black Spider T-shirt Black Spider T-shirt Black Spider T-shirt Black Spider T-shirt Black Spider T-shirt Black Spider T-shirt ', 'images/all_products/men/t-shirt/MTS0220182691519642092t_shirt4.jpg', 'men', 't-shirt'),
(10, 'MTS0220182610', 'Red Diamond Design T-shirt', 100, 'Red Diamond Design T-shirt Red Diamond Design T-shirt Red Diamond Design T-shirt Red Diamond Design T-shirt ', 'images/all_products/men/t-shirt/MTS02201826101519642243t_shirt2.jpg', 'men', 't-shirt'),
(11, 'MFP0220182611', 'Blue Pant', 1500, 'Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant Blue Pant ', 'images/all_products/men/formal_pant/MFP02201826111519642323Pant02.jpeg', 'men', 'formal_pant'),
(12, 'SSS0220182612', 'Addidash Sports Shoes', 2500, 'Addidash Sports Shoes Addidash Sports Shoes Addidash Sports Shoes Addidash Sports Shoes Addidash Sports Shoes ', 'images/all_products/sports/shorts_shoes/SSS02201826121519642409shoes02.jpg', 'sports', 'shorts_shoes'),
(13, 'SSS0220182613', 'Black Casual Shoes', 1200, 'Black Casual Shoes Black Casual Shoes Black Casual Shoes Black Casual Shoes ', 'images/all_products/sports/shorts_shoes/SSS02201826131519642803shoes01.jpg', 'sports', 'shorts_shoes');

-- --------------------------------------------------------

--
-- Table structure for table `main_category`
--

CREATE TABLE `main_category` (
  `sl_id` int(2) NOT NULL,
  `main_categ_name` varchar(50) NOT NULL,
  `main_categ_folder` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_category`
--

INSERT INTO `main_category` (`sl_id`, `main_categ_name`, `main_categ_folder`) VALUES
(4, 'Men', 'men'),
(5, 'Women', 'women'),
(6, 'Kids', 'kids'),
(7, 'Home Accessories', 'home_accessories'),
(8, 'Baby Care Center', 'baby_care_center'),
(9, 'Sports', 'sports'),
(10, 'Travel', 'travel');

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE `student_registration` (
  `sl_id` int(3) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `stu_father_name` varchar(50) NOT NULL,
  `stu_mother_name` varchar(50) NOT NULL,
  `stu_dob` varchar(12) NOT NULL,
  `stu_gender` varchar(2) NOT NULL,
  `stu_phone` varchar(17) NOT NULL,
  `stu_email` varchar(100) NOT NULL,
  `stu_nid` varchar(24) NOT NULL,
  `stu_mstatus` varchar(3) NOT NULL,
  `stu_bgroup` varchar(3) NOT NULL,
  `stu_div` varchar(5) NOT NULL,
  `stu_course` varchar(2) NOT NULL,
  `stu_fburl` text NOT NULL,
  `stu_foccu` varchar(20) NOT NULL,
  `stu_paddr` text NOT NULL,
  `stu_prmaddr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`sl_id`, `student_name`, `stu_father_name`, `stu_mother_name`, `stu_dob`, `stu_gender`, `stu_phone`, `stu_email`, `stu_nid`, `stu_mstatus`, `stu_bgroup`, `stu_div`, `stu_course`, `stu_fburl`, `stu_foccu`, `stu_paddr`, `stu_prmaddr`) VALUES
(4, 'Manik Sarkar', 'Wali Ullah Sarkar', 'Nazma Sarker', '10-10-2010', 'M', '+8801683432790', 'demo@demo.com', '123456789012', 'Mr', 'B+', 'dhk', '--', 'http://localhost/', 'Retired', '35/3, North Golapbagh', '35/3, North Golapbagh'),
(8, 'Md. Sharif Ullah Sarkar', 'Md. Wali Ullah Sarkar', 'Nazma Sarker', '18-2-2003', 'M', '01683432790', 'youthict00@gmail.com', '123456789012', 'Mr', 'A+', 'dhk', 'B-', 'http://localhost/TUP-OFF-BDJ-19/php_class03-10-01-2018/registrationpage.php', 'Retired', '35/3, North Golapbagh', '35/3, North Golapbagh');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `sl_id` int(6) NOT NULL,
  `sub_categ_name` varchar(35) NOT NULL,
  `sub_categ_folder` varchar(40) NOT NULL,
  `main_categ_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`sl_id`, `sub_categ_name`, `sub_categ_folder`, `main_categ_name`) VALUES
(1, 'T-Shirt', 't-shirt', 'men'),
(2, 'Washing Machine', 'washing_machine', 'home_accessories'),
(3, 'Pakistani Lawn', 'pakistani_lawn', 'women'),
(4, 'Formal Shirt', 'formal_shirt', 'men'),
(5, 'Formal Pant', 'formal_pant', 'men'),
(26, 'Wooden Furniture', 'wooden_furniture', 'home_accessories'),
(27, 'Shorts Shoes', 'shorts_shoes', 'sports'),
(28, 'Casual Shirt', 'casual_shirt', 'men'),
(29, 'Diamond Neckless', 'diamond_neckless', 'women');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_access`
--
ALTER TABLE `admin_access`
  ADD PRIMARY KEY (`sl_id`),
  ADD UNIQUE KEY `admin_email` (`admin_email`);

--
-- Indexes for table `all_products`
--
ALTER TABLE `all_products`
  ADD PRIMARY KEY (`sl_id`);

--
-- Indexes for table `main_category`
--
ALTER TABLE `main_category`
  ADD PRIMARY KEY (`sl_id`),
  ADD UNIQUE KEY `main_categ_name` (`main_categ_name`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`sl_id`),
  ADD UNIQUE KEY `stu_email` (`stu_email`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`sl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_access`
--
ALTER TABLE `admin_access`
  MODIFY `sl_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `all_products`
--
ALTER TABLE `all_products`
  MODIFY `sl_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `main_category`
--
ALTER TABLE `main_category`
  MODIFY `sl_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `sl_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `sl_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
