<style>
#product_nav .navbar-inverse{
	background-color:#af4726;
	border:0;
	border-radius: 0px; 
	min-height: 30px;
}
#productNavbar ul li a{
	padding-top:10px;
	padding-bottom:10px;
	color: yellow;
}
#productNavbar ul li.active a{
	background-color: #6b1202;
	color: white;
}

</style>
<section id="product_nav">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#productNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="productNavbar">
      <ul class="nav navbar-nav">
<?php
############## Main Categories View ################
	require_once('class_lib/main_category_class.php');
	$main_categ_obj= new Main_Category;
	/////////// for Sub Category form
	$main_categ_data=$main_categ_obj->main_categ_view();
	
	if($main_categ_data->num_rows >0){
			$x=1;
			while($main_categ_list=$main_categ_data->fetch_assoc()){
				$main_categ_name=$main_categ_list['main_categ_name'];
				$main_categ_value=$main_categ_list['main_categ_folder'];
				///print_r($main_categ_list);
				
				############# Sub Category Table Start
				require_once('class_lib/sub_category_class.php');
				$sub_categ_obj= new Sub_Category;
				$Sub_category_table=$sub_categ_obj->sub_categ_view_main($main_categ_value);
				$sub_category_row=$Sub_category_table->num_rows;
				?>
				<li class="dropdown <?php if(isset($_GET['main_id']) && $_GET['main_id']== $main_categ_value){ echo 'active';} ?>">
				  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $main_categ_name;  ?><span class="caret <?php if($sub_category_row==0){ echo 'sr-only';}?>"></span></a>
				  <ul class="dropdown-menu <?php if($sub_category_row==0){ echo 'sr-only';}?>" style="background-color: #af4726;">
			<?php
				
				
				if($sub_category_row > 0){
					while($sub_categ_list=$Sub_category_table->fetch_assoc()){
						$sub_categ_name=$sub_categ_list['sub_categ_name'];
						$sub_categ_value=$sub_categ_list['sub_categ_folder'];
						///print_r($sub_categ_list);
						?>
						<li class="<?php if(isset($_GET['sub_id']) && $_GET['sub_id']==$sub_categ_value){ echo 'active';} ?>" ><a href="subcategory_product.php?main_id=<?php echo $main_categ_value?> && sub_id=<?php echo $sub_categ_value;?>"><?php echo $sub_categ_name; ?></a></li>
						<?php
					}
				}
			?>
				  </ul>
				</li>
				
				<?php
			}
	}

?>
      </ul>
    </div>
  </div>
</nav>
</section>