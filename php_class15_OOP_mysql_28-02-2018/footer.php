	<footer id="footer" style="background-color:blue;">
		<div class="container">
			<div class="col-md-2 col-sm-2 col-xs-4">
				<img class="img-responsive" src="images/logo.png" width="50" alt="Company Logo">
			</div>
			<div class="col-md-3 col-sm-4 col-xs-8 pull-right hsocialicon">
				<ul class="list-inline">
					<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="col-md-7 col-sm-6 col-xs-12 pull-left">
				<h4>&copy; 2018 md.sharif ullah sarkar</h4>
			</div>
		</div>
	</footer>




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/lightbox.min.js"></script>
	<script>
		$('#main_slider').carousel({
		  interval: 4000
		})
	</script>
  </body>
</html>
<?php
	ob_flush();
	ob_clean();
?>