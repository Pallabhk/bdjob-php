<?php
	
	require_once('db_connection_class.php');
	
	class Admin_Access extends DB_CONNECT{
		
		public function admin_login($data){
			$ad_email=$data['a_email'];
			$ad_pass=$data['a_pass'];
			
			
			$sql_check="SELECT * FROM admin_access WHERE admin_email='$ad_email' && admin_pass='$ad_pass'";
			
			$db_connt=$this->connect;
			
			$result=$db_connt->query($sql_check);
			
			if($db_connt->error){
				echo 'Error:' .$db_connt->error;
			}else{
				if($result->num_rows ==0){
					echo '<div class="alert alert-warning text-center" role="alert">Invalid Email Or Password</div>';
					header('refresh:2; url=index.php');
				}else{
					$admin_data=$result->fetch_assoc();
					$admin_name=$admin_data['admin_name'];
					$admin_access=$admin_data['admin_action'];
					
					if($admin_access=='root_admin'){
						################ Cookies Set ################
						if(isset($data['a_remb'])){
							$ad_remb=$data['a_remb'];
							if($ad_remb=='Y'){
								setcookie('user_email',$ad_email,time()+5*60,'/');
								setcookie('user_pass',$ad_pass,time()+5*60,'/');
							}
							
						}else{
							setcookie('user_email','',0,'/');
							setcookie('user_pass','',0,'/');
						}
						############### Session Creat ##############
						$_SESSION['r_admin_name']=$admin_name;
						header('Location: r_admin.php');
						
					}else if($admin_access=='admin'){
						
						################ Cookies Set ################
						if(isset($data['a_remb'])){
							$ad_remb=$data['a_remb'];
							if($ad_remb=='Y'){
								setcookie('user_email',$ad_email,time()+5*60,'/');
								setcookie('user_pass',$ad_pass,time()+5*60,'/');
							}
							
						}else{
							setcookie('user_email','',0,'/');
							setcookie('user_pass','',0,'/');
						}
						############### Session Creat ##############

						$_SESSION['admin_name']=$admin_name;
						header('Location: admin.php');
					}
				
				}
			}
			
		
			
		}//admin_login
		
		public function admin_logout(){
			session_unset();
			session_destroy();
			header('Location: index.php');
		}
	}// class
	
	
	
	
	
	
	
	
	
	
	
	
	
?>