<?php
	session_start();
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<header>
		<nav class="navbar navbar-default">
		  <div class="container">
			<div class="col-sm-2 col-xs-5 navbar-header">
			  <a class="navbar-brand" style="padding:5px 15px;" href="#">
				<img alt="Brand" src="../images/logo.png" class="img-responsive" style="height:40px;">
			  </a>
			</div>
			<div class="col-sm-2 col-xs-7 pull-right">
				<button type="button" class="btn btn-info navbar-btn pull-right" data-toggle="modal" data-target="#a_logout">Log Out <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></button>
			</div>
			<div class="col-sm-8 col-xs-12 pull-left">
			<!-- ============ Admin Name ============ -->
				<h3 class="text-center">
					<?php 
						if(isset($_SESSION['r_admin_name'])){
							echo $_SESSION['r_admin_name']." **Root Admin";
						}else if(isset($_SESSION['admin_name'])){
							echo $_SESSION['admin_name']." **Admin";
						}
					?>
				</h3>
			</div>
		  </div>
		</nav>
<!-- ================== Log Out Modal ===================== -->
<div id="a_logout"class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style="padding:25px 10px;">
      <h4 class="text-center">Do you want to Log Out ?</h4>
	  <div class="text-center">
		  <a role="button" href="?logout=logout" class="btn btn-danger">Yes</a>
		  <?php
			if(isset($_REQUEST['logout'])){
				require_once('../class_lib/admin_access_class.php');

				$logout_obj = new Admin_Access;

				$logout_obj->admin_logout();
			}
		  ?>
		  
		  <button type="button" class="btn btn-info" data-dismiss="modal">NO</button>
	  </div>
    </div>
  </div>
</div>
<!-- ================== Log Out Modal Close===================== -->	
	</header>
	
	
<!-- ===============###########=== Content Part ======################=============== -->
	<div class="container-fluid">
		<div class="row">
		<!-- ===============###########=== Content Navbar Start ======################=============== -->	
			<div class="col-sm-4">
				<ul class="nav nav-pills nav-stacked">
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='r_admin.php'){ echo 'active'; }?>"><a href="r_admin.php">DashBoard</a></li>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='add_category.php'){ echo 'active'; }?>"><a href="add_category.php">Add Category</a></li>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='add_category.php'){ echo 'active'; }?>"><a href="add_product.php">Add Product</a></li>
				  <li><a href="#">Add Product</a></li>
				  <li><a href="#">Sessional Offers</a></li>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='add_admin.php'){ echo 'active'; }?> <?php if($_SESSION['admin_name']){ echo 'disabled'; } ?>"><a href="add_admin.php">Add Admin</a></li>
				</ul>
			
			</div>
			
			
			
			
			
			
			
