<?php
	ob_start();
 include('header.php');


?>
		<!-- ===============###########=== Content Part Start ======################=============== -->	
			<div class="col-sm-8">
			
				<!-- ======================== Add Main Categories =============== -->
				<h3 class="alert alert-info text-center">Add Main Categories</h3>
<?php
	if(isset($_POST['main_categ_submit']) && !empty($_POST['main_categ_name'])){
		require_once('../class_lib/main_category_class.php');
		$main_categ_obj= new Main_Category;
		$main_categ_obj->main_categ_insert($_POST);
	}

?>			
				<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				
				  <div class="form-group">
					<label for="main_categ_name">Add Main Category</label>
					<input name="main_categ_name" type="text" class="form-control" id="main_categ_name"  placeholder="Add Main Category">
				  </div>
				  
				  <div class="form-group">
				  <button name="main_categ_submit" type="submit" class="btn btn-info">Add Main Category</button>
				  </div>
				</form>
				<br>
				<hr style="width:100%;">
				
				<!-- ======================== Add Sub Categories =============== -->
				<h3 class="alert alert-info text-center">Add Sub Categories</h3>
<?php
	require_once('../class_lib/main_category_class.php');
	$main_categ_obj= new Main_Category;

	$main_categ_data=$main_categ_obj->main_categ_view();
	### Table category####
	$main_categ_data_tbl=$main_categ_obj->main_categ_view();

	
	############### Sub Category Insert ############
	if(isset($_POST['sub_categ_submit']) && !empty($_POST['main_categ']) && !empty($_POST['sub_categ_name'])){
		require_once('../class_lib/sub_category_class.php');
		$sub_categ_obj= new Sub_Category;
		$sub_categ_obj->sub_categ_insert($_POST);
	}else{
		if(isset($_POST['sub_categ_submit'])){
			echo '<div class="alert alert-warning text-center" role="alert">Please fill-up all sub category fields</div>';
		}
	}

?>
				<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				  <div class="form-group">
					<label for="main_categ">Choose Main Category</label>
					<select name="main_categ" class="form-control" id="main_categ">
						<option value="">Choose Main Category</option>
				<?php
					if($main_categ_data->num_rows>0){
						while($main_categ_list=$main_categ_data->fetch_assoc()){
							
							echo '<option value="'.$main_categ_list['main_categ_folder'].'">'.$main_categ_list['main_categ_name'].'</option>';
							
						}
					}else{
						echo '<option value="">There have no Main Category</option>';
					}
				
				?>
					</select>
				  </div>
				  <div class="form-group">
					<label for="sub_categ_name">Add sub Category</label>
					<input name="sub_categ_name" type="text" class="form-control" id="sub_categ_name"  placeholder="Add Sub Category">
				  </div>
				  <div class="form-group">
				  <button name="sub_categ_submit" type="submit" class="btn btn-success">Add Sub Category</button>
				  </div>
				</form>
				
				<br>
				<hr style="width:100%;">
				
				<h3 class="alert alert-warning text-center">Category Table</h3>
					<table class="table table-striped">
						<tr>
							<th>Sl</th>
							<th>Main Category</th>
							<th>Sub Category</th>
						</tr>

						<?php

							if ($main_categ_data_tbl->num_rows >0) {

								$x=1;
								while ($main_categ_list =$main_categ_data_tbl->fetch_assoc()) {
									
									$main_categ_name =$main_categ_list['main_categ_name'];
									$main_categ_value =$main_categ_list['main_categ_folder'];
									
									//print_r('$main_categ_list');
								}
								?>

							<tr>
								<td><?php echo $x++;?></td>
								<td><?php echo $main_categ_name?></td>


								<td>Sub Category</td>
							</tr>
							<?php }else{?>
								<tr>
									<td colspan="3" class="text-center">There have no main sub Category </td>
								</tr>
							<?php }
						?>
						
					</table>

				

			
			</div><!-- Content div -->
		
		<!-- ===============###########=== Content Part close ======################=============== -->	
		
<?php include('footer.php'); ?>
