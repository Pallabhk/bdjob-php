<?php
	include('db_connection.php');
	
	function student_reg($data){

		$a_fullname=$data['fullname'];
		$a_fname=$data['fathername'];
		$a_mname=$data['mothername'];
		$a_bday=$data['b_day'];
		$a_bmth=$data['b_mth'];
		$a_byr=$data['b_yr'];
		$a_gender=$data['gender'];
		$a_mnumber=$data['mnumber'];
		$a_email=$data['email'];
		$a_nid=$data['nid'];
		$a_mstatus=$data['mstatus'];
		$a_bgroup=$data['b_group'];
		$a_div=$data['division'];
		if(isset($data['courses1'])){
			$a_cour1=htmlspecialchars($data['courses1']);
		}
		if(isset($data['courses2'])){
			$a_cour2=htmlspecialchars($data['courses2']);
		}
		if(isset($data['courses3'])){
			$a_cour3=htmlspecialchars($data['courses3']);
		}
		if(isset($data['courses4'])){
			$a_cour4=htmlspecialchars($data['courses4']);
		}
		if(isset($data['courses5'])){
			$a_cour5=htmlspecialchars($data['courses5']);
		}
		$a_fburl=$data['fb_url'];
		$a_foccu=$data['foccu'];
		$a_praddr=$data['praddr'];
		$a_prmaddr=$data['prmaddr'];
		
		$error='';
		########### Validation all field ############
		
		######## Applicant Full Name ############
		
		if(strlen($a_fullname)>=6 && strlen($a_fullname)<=50 && str_word_count($a_fullname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fullname)){
			echo $a_fullname.'<br>';
		}
		else{
			$error.= 'Full name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Father's Name ############
		
		if(strlen($a_fname)>=6 && strlen($a_fname)<=50 && str_word_count($a_fname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fname)){
			echo $a_fname.'<br>';
		}
		else{
			$error.= 'Father name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Mother's Name ############
		
		if(strlen($a_mname)>=6 && strlen($a_mname)<=50 && str_word_count($a_mname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_mname)){
			echo $a_mname.'<br>';
		}
		else{
			$error.= 'Mother name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		
		######## Applicant's Date of Birth ############
		if($a_bday>0 && $a_bday<=31 && $a_bmth >0 && $a_bmth<=12 && $a_byr>=1980 && $a_byr<=2018 ){
			echo $a_bday.'-'.$a_bmth.'-'.$a_byr.'<br>';
		}
		else{
			$error.= 'Your Birthdate is not Correct. <br>';
		}
		
		######## Applicant's Gender ############
		if($a_gender ==='M' || $a_gender ==='F' ){
			echo $a_gender.'<br>';
		}else{
			$error.= 'Gender does not matched. <br>';
		}
		
		######## Applicant's Phone Number ############
		if(strlen($a_mnumber)>=11 && strlen($a_mnumber)<=17 && preg_match('/^[-0-9.+ ]*$/',$a_mnumber)){
			echo $a_mnumber.'<br>';
		}else{
			$error.= 'Phone Number will be like this +8801683432790 <br>';
		}
		
		######## Applicant Email ############
		if(filter_var($a_email, FILTER_VALIDATE_EMAIL)){
			echo $a_email.'<br>';
		}
		else{
			$error.= 'Invalid Email..<br>';
		}
		
		######## Applicant's NID Number ############
		if(strlen($a_nid)>=10 && strlen($a_nid)<=22 && preg_match('/^[0-9]*$/',$a_nid)){
			echo $a_nid.'<br>';
		}else{
			$error.= 'NID Number will be like this 2345671234 <br>';
		}
		
		######## Applicant's Marital Status ############
		if($a_mstatus ==='Mr' || $a_mstatus ==='Umr' ){
			echo $a_mstatus.'<br>';
		}else{
			$error.= 'Marital Status does not matched <br>';
		}
		
		######## Applicant's Blood Group ############
		if($a_bgroup ==='A+' || $a_bgroup ==='A-' || $a_bgroup ==='B+' || $a_bgroup ==='B-' || $a_bgroup ==='O+' || $a_bgroup ==='O-' || $a_bgroup ==='AB+' || $a_bgroup ==='AB-' ){
			echo $a_bgroup.'<br>';
		}else{
			$error.= 'Blood Group does not matched <br>';
		}
		
		######## Applicant's Division Area ############
		if($a_div ==='dhk' || $a_div ==='ctg' || $a_div ==='raj' || $a_div ==='syl' || $a_div ==='rang' || $a_div ==='bar' || $a_div ==='khu'){
			echo $a_div.'<br>';
		}else{
			$error.= 'Division Value does not matched <br>';
		}
		
		
		######## Applicant's Division Area ############
		if(isset($a_cour1)){
			if($a_cour1==='B'){
			echo $a_cour1;
			}else{ 
				$error.= 'Unknown Bangla Course Value <br>';
			}
		}
		if(isset($a_cour2)){
			if($a_cour2==='E'){
				echo $a_cour2;
			}else{ 
				$error.= 'Unknown English Course Value <br>';
			}
		}
		if(isset($a_cour3)){
			if($a_cour3==='M'){
				echo $a_cour3;
			}else{ 
				$error.= 'Unknown Math Course Value <br>';
			}
		}
		if(isset($a_cour4)){
			if($a_cour4==='I'){
				echo $a_cour4;
			}else{ 
				$error.= 'Unknown Islamic Course Value <br>';
			}
		}
		if(isset($a_cour5)){
			if($a_cour5==='C'){
				echo $a_cour5;
			}else{ 
				$error.= 'Unknown Cultural Course Value <br>';
			}
		}
		
		
		########### Applicant Fb Url ############
		if(filter_var($a_fburl, FILTER_VALIDATE_URL)){
			echo $a_fburl.'<br>';
		}
		else{
			$error.= 'Invalid FB Url..<br>';
		}
		
		########### Applicant Father's Occupation ############
		if(strlen($a_foccu)>=4 && strlen($a_foccu)<=20 && str_word_count($a_foccu)>=1 && preg_match('/^[-a-zA-Z. ]*$/',$a_foccu)){
			echo $a_foccu.'<br>';
		}
		else{
			$error.= 'Father Occupation must be with in 4-20 letters..<br>';
		}
		
		########### Applicant Present Address ############
		if(strlen($a_praddr)>=4 && strlen($a_praddr)<=100 && str_word_count($a_praddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_praddr)){
			echo $a_praddr.'<br>';
		}
		else{
			$error.= 'Address must be with in 4-100 letters..<br>';
		}
		
		########### Applicant Parmanent Address ############
		if(strlen($a_prmaddr)>=4 && strlen($a_prmaddr)<=100 && str_word_count($a_prmaddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_prmaddr)){
			echo $a_prmaddr.'<br>';
		}
		else{
			$error.= 'Parmanent Address must be with in 4-100 letters..<br>';
		}
		
		
		
		############### Inserting Process ############
		
		if(!$error){
			
			//// date of birth
			$a_dob= $a_bday.'-'.$a_bmth.'-'.$a_byr;
			///// Selected Courses
			$course=$a_cour1.'-'.$a_cour2.'-'.$a_cour3.'-'.$a_cour4.'-'.$a_cour5;
			
			$connt=db_connect();
			
			$sql_insert="INSERT INTO student_registration(student_name, stu_father_name, stu_mother_name, stu_dob, stu_gender, stu_phone, stu_email, stu_nid, stu_mstatus, stu_bgroup, stu_div, stu_course, stu_fburl, stu_foccu, stu_paddr, stu_prmaddr) VALUES ('$a_fullname','$a_fname','$a_mname','$a_dob','$a_gender','$a_mnumber','$a_email','$a_nid','$a_mstatus', '$a_bgroup','$a_div','$course','$a_fburl','$a_foccu','$a_praddr','$a_prmaddr' )";
			
			$result=mysqli_query($connt,$sql_insert);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				echo 'Data Insert Successfully';
			}
		
		}else{
			echo 'Error: <br>'.$error;
		}
		
	}
	
	###################### Update User Data ############################
	function student_info_update($data){
		$a_serial_id=$data['sl_id'];
		$a_fullname=$data['fullname'];
		$a_fname=$data['fathername'];
		$a_mname=$data['mothername'];
		$a_bday=$data['b_day'];
		$a_bmth=$data['b_mth'];
		$a_byr=$data['b_yr'];
		$a_gender=$data['gender'];
		$a_mnumber=$data['mnumber'];
		$a_nid=$data['nid'];
		$a_mstatus=$data['mstatus'];
		$a_bgroup=$data['b_group'];
		$a_div=$data['division'];
		if(isset($data['courses1'])){
			$a_cour1=htmlspecialchars($data['courses1']);
		}
		if(isset($data['courses2'])){
			$a_cour2=htmlspecialchars($data['courses2']);
		}
		if(isset($data['courses3'])){
			$a_cour3=htmlspecialchars($data['courses3']);
		}
		if(isset($data['courses4'])){
			$a_cour4=htmlspecialchars($data['courses4']);
		}
		if(isset($data['courses5'])){
			$a_cour5=htmlspecialchars($data['courses5']);
		}
		$a_fburl=$data['fb_url'];
		$a_foccu=$data['foccu'];
		$a_praddr=$data['praddr'];
		$a_prmaddr=$data['prmaddr'];
		
		$error='';
		########### Validation all field ############
		
		######## Applicant Full Name ############
		
		if(strlen($a_fullname)>=6 && strlen($a_fullname)<=50 && str_word_count($a_fullname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fullname)){
			echo $a_fullname.'<br>';
		}
		else{
			$error.= 'Full name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Father's Name ############
		
		if(strlen($a_fname)>=6 && strlen($a_fname)<=50 && str_word_count($a_fname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fname)){
			echo $a_fname.'<br>';
		}
		else{
			$error.= 'Father name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Mother's Name ############
		
		if(strlen($a_mname)>=6 && strlen($a_mname)<=50 && str_word_count($a_mname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_mname)){
			echo $a_mname.'<br>';
		}
		else{
			$error.= 'Mother name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		
		######## Applicant's Date of Birth ############
		if($a_bday>0 && $a_bday<=31 && $a_bmth >0 && $a_bmth<=12 && $a_byr>=1980 && $a_byr<=2018 ){
			echo $a_bday.'-'.$a_bmth.'-'.$a_byr.'<br>';
		}
		else{
			$error.= 'Your Birthdate is not Correct. <br>';
		}
		
		######## Applicant's Gender ############
		if($a_gender ==='M' || $a_gender ==='F' ){
			echo $a_gender.'<br>';
		}else{
			$error.= 'Gender does not matched. <br>';
		}
		
		######## Applicant's Phone Number ############
		if(strlen($a_mnumber)>=11 && strlen($a_mnumber)<=17 && preg_match('/^[-0-9.+ ]*$/',$a_mnumber)){
			echo $a_mnumber.'<br>';
		}else{
			$error.= 'Phone Number will be like this +8801683432790 <br>';
		}
		
		
		######## Applicant's NID Number ############
		if(strlen($a_nid)>=10 && strlen($a_nid)<=22 && preg_match('/^[0-9]*$/',$a_nid)){
			echo $a_nid.'<br>';
		}else{
			$error.= 'NID Number will be like this 2345671234 <br>';
		}
		
		######## Applicant's Marital Status ############
		if($a_mstatus ==='Mr' || $a_mstatus ==='Umr' ){
			echo $a_mstatus.'<br>';
		}else{
			$error.= 'Marital Status does not matched <br>';
		}
		
		######## Applicant's Blood Group ############
		if($a_bgroup ==='A+' || $a_bgroup ==='A-' || $a_bgroup ==='B+' || $a_bgroup ==='B-' || $a_bgroup ==='O+' || $a_bgroup ==='O-' || $a_bgroup ==='AB+' || $a_bgroup ==='AB-' ){
			echo $a_bgroup.'<br>';
		}else{
			$error.= 'Blood Group does not matched <br>';
		}
		
		######## Applicant's Division Area ############
		if($a_div ==='dhk' || $a_div ==='ctg' || $a_div ==='raj' || $a_div ==='syl' || $a_div ==='rang' || $a_div ==='bar' || $a_div ==='khu'){
			echo $a_div.'<br>';
		}else{
			$error.= 'Division Value does not matched <br>';
		}
		
		
		######## Applicant's Division Area ############
		if(isset($a_cour1)){
			if($a_cour1==='B'){
			echo $a_cour1;
			}else{ 
				$error.= 'Unknown Bangla Course Value <br>';
			}
		}
		if(isset($a_cour2)){
			if($a_cour2==='E'){
				echo $a_cour2;
			}else{ 
				$error.= 'Unknown English Course Value <br>';
			}
		}
		if(isset($a_cour3)){
			if($a_cour3==='M'){
				echo $a_cour3;
			}else{ 
				$error.= 'Unknown Math Course Value <br>';
			}
		}
		if(isset($a_cour4)){
			if($a_cour4==='I'){
				echo $a_cour4;
			}else{ 
				$error.= 'Unknown Islamic Course Value <br>';
			}
		}
		if(isset($a_cour5)){
			if($a_cour5==='C'){
				echo $a_cour5;
			}else{ 
				$error.= 'Unknown Cultural Course Value <br>';
			}
		}
		
		
		########### Applicant Fb Url ############
		if(filter_var($a_fburl, FILTER_VALIDATE_URL)){
			echo $a_fburl.'<br>';
		}
		else{
			$error.= 'Invalid FB Url..<br>';
		}
		
		########### Applicant Father's Occupation ############
		if(strlen($a_foccu)>=4 && strlen($a_foccu)<=20 && str_word_count($a_foccu)>=1 && preg_match('/^[-a-zA-Z. ]*$/',$a_foccu)){
			echo $a_foccu.'<br>';
		}
		else{
			$error.= 'Father Occupation must be with in 4-20 letters..<br>';
		}
		
		########### Applicant Present Address ############
		if(strlen($a_praddr)>=4 && strlen($a_praddr)<=100 && str_word_count($a_praddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_praddr)){
			echo $a_praddr.'<br>';
		}
		else{
			$error.= 'Address must be with in 4-100 letters..<br>';
		}
		
		########### Applicant Parmanent Address ############
		if(strlen($a_prmaddr)>=4 && strlen($a_prmaddr)<=100 && str_word_count($a_prmaddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_prmaddr)){
			echo $a_prmaddr.'<br>';
		}
		else{
			$error.= 'Parmanent Address must be with in 4-100 letters..<br>';
		}
		
		
		
		############### Inserting Process ############
		
		if(!$error){
			
			//// date of birth
			$a_dob= $a_bday.'-'.$a_bmth.'-'.$a_byr;
			///// Selected Courses
			$course=$a_cour1.'-'.$a_cour2.'-'.$a_cour3.'-'.$a_cour4.'-'.$a_cour5;
			
			$connt=db_connect();
			
			$sql_update="UPDATE student_registration SET student_name='$a_fullname', stu_father_name='$a_fname', stu_mother_name='$a_mname', stu_dob='$a_dob', stu_gender='$a_gender', stu_phone='$a_mnumber', stu_nid='$a_nid', stu_mstatus='$a_mstatus', stu_bgroup='$a_bgroup', stu_div='$a_div', stu_course='$course', stu_fburl='$a_fburl', stu_foccu='$a_foccu', stu_paddr='$a_praddr', stu_prmaddr='$a_prmaddr' WHERE sl_id='$a_serial_id'";
			
			$result=mysqli_query($connt,$sql_update);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				echo 'Data Update Successfully';
				header('Location: index.php');
			}
		
		}else{
			echo 'Error: <br>'.$error;
		}
		
	}
	
	############# View All Student Data Table #########
	
	function student_data_table(){
		$connt=db_connect();
		$sql_view="SELECT * FROM student_registration";
		
		$result=mysqli_query($connt,$sql_view);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				return $result;
			}
	}
	############# View Each Student Data Table #########
	
	function student_data_info($data){
		$connt=db_connect();
		$sql_view="SELECT * FROM student_registration WHERE sl_id='$data'";
		
		$result=mysqli_query($connt,$sql_view);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				return $result;
			}
	}
	
	############# Student Data Delete #########
	
	function student_data_delete($data){
		$connt=db_connect();
		$sql_del="DELETE FROM student_registration WHERE sl_id='$data'";
		
		$result=mysqli_query($connt,$sql_del);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				echo 'Data Delet Successfully';
				header('Location:index.php');
			}
	}
	

	
	
?>
