<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Md. Sharif Ullah Sarkar">
    <meta name="description" content="Portfolio">
    <meta name="keywords" content="HTML5, CSS3, Web Design, Web Development, Responsive Web Design, Bootstrap, OOP php, Raw Php, Wordpress, Laravel, Dynamic Website, Low price Dynamic website.">
	
	<!-- company icon -->
	<link href="images/icon/apple-touch-icon.png" rel="apple-touch-icon" size="180x180">
	<link href="images/icon/favicon.ico" rel="icon">
	<link rel="icon" href="images/icon/logo.png" size="32x32">
	
	<!-- company Name/Title -->
    <title>Md. Sharif Ullah Sarkar</title>

    <!--  All CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/lightbox.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<header id="headercont">
		<div class="container">
			<div class="col-md-2 col-sm-2 col-xs-4">
				<img class="img-responsive" src="images/logo.png" width="75" alt="Company Logo">
			</div>
			<div class="col-md-3 col-sm-4 col-xs-8 pull-right hsocialicon">
				<ul class="list-inline">
					<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="col-md-7 col-sm-6 col-xs-12 pull-left">
				<h1>Md. Sharif Ullah Sarkar</h1>
			</div>
		</div>
<nav class="navbar navbar-inverse" style="margin-bottom:0;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='index.php'){ echo 'active';}?>"><a href="index.php">Home</a></li>
        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='product.php'){ echo 'active';}?>"><a href="product.php">Product</a></li>
        <li><a href="about_us.php">About Us</a></li>
        <li class="<?php if(basename($_SERVER['PHP_SELF'])=='registraion.php'){ echo 'active';}?>"><a href="registraion.php">Registraion</a></li>
        <li><a href="contact_us.php">Contact Us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#" data-toggle="modal" data-target="#userlogin"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
	</header>
<!-- login Modal -->
	<div class="modal fade" id="userlogin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		  <form>
			  <div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
			  </div>
			  <div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>
			  <div class="checkbox">
				<label>
				  <input type="checkbox"> Check me out
				</label>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	  </div>
	</div>
