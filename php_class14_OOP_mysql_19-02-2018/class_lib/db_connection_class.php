<?php
	
	require_once(__DIR__.'/../db_details.php');
	
	class DB_CONNECT{
		
		private $db_host=DB_HOST;
		private $db_user=DB_USER;
		private $db_pass=DB_PASS;
		private $db_name=DB_NAME;
		
		protected $connect;
		
		public function __construct(){
			
			$this->db_conn();
		}
		
		private function db_conn(){
			
			$this->connect= new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
			
			if($this->connect->connect_error){
				echo die('Error: '.$this->connect->connect_error);
			}else{
				return $this->connect;
			}
			
		}
		
		
		
		
		
		
	}
	
	


?>