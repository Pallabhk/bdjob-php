<?php
	include('header.php');
	//require('header.php');

?>
<style>
	
	#all_product a{
		text-decoration:none;
	}
	#all_product img{
		height: 200px;
	}
	#all_product p{
		border-top:1px solid gray;
		padding-top: 10px;
		font-size: 1.2em;
	}
</style>
<!-- ============= Body Content Part ================ -->
<!-- ============= Product NavBar ================ -->
<?php include('productNav.php');?>
<!-- ============= Body Content Part ================ -->
<section id="all_product">
	<div class="container">
		<div class="row">
<?php
	require_once('class_lib/user_product_view_class.php');
	$product_obj= new userPRODUCTview;
	$product=$product_obj->all_product_view();
	
	if($product->num_rows > 0){
		while($p_data=$product->fetch_assoc()){
			///print_r($p_data);
			
			$product_img=$p_data['product_image'];
			$product_name=$p_data['product_name'];
			$product_price=$p_data['product_price'];
			$product_code=$p_data['product_code'];
		?>
			<div class="col-sm-3 col-xs-6">
				<a href="product_detail.php?p_code=<?php echo $product_code; ?>" class="thumbnail">
				  <img src="<?php echo $product_img; ?>" alt="<?php echo $product_name; ?>">
				  <div class="caption text-center">
					<h3><?php echo $product_name; ?></h3>
					<p><?php echo $product_price; ?>/-tk</p>
					<p><?php echo $product_code; ?></p>
				  </div>
				</a>
			</div>		
		<?php		
		}

	}

?>

		</div>
	</div>
</section>
<!-- ============ Footer Part ============= -->
<?php
	require'footer.php';
?>