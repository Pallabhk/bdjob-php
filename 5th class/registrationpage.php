
<!DOCTYPE html>
<html>
	<head>
		<title>Registration</title>
	</head>
	
	<body>
		<form action="reg_validation.php" method="post" style="width:600px; margin:auto;">
			<fieldset>
				<legend><mark>Registration Form</mark></legend>
				<table>
					<tr><!-- Row 01 -->
						<td>Name:</td>
						<td><input name="stu_name" type="text" placeholder="Write your Full Name"></td>
					</tr>
					<tr><!-- Row 02 -->
						<td>Father's Name:</td>
						<td><input name="stu_fathername" type="text" placeholder="Write your Father Name"></td>
					</tr>
					<tr><!-- Row 03 -->
						<td>Mother's Name:</td>
						<td><input name="stu_mothername" type="text" placeholder="Write your Mother Name"></td>
					</tr>
					<tr><!-- Row 04 -->
						<td>Date of Birth</td>
						<td>Day:<select name="stu_dob">
								<option value="">Day</option>
									<?php
										for($day=1; $day<=31;$day++){
											
											echo '<option value="'.$day.'">'.$day.'</option>';
										}
									?>
							</select>
							Month:
							<select name="b_mth">
								<option value="">month</option>
									<?php
										$month=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');

										foreach($month as $mvalue=>$mname ){
											echo '<option value="'.++$mvalue.'">'.$mname.'</option>';
										}
									
									?>
							</select>
							Year:
							<select name="b_yr">
								<option value="">year</option>
								<?php
									for($year=2018; $year>=1980;$year--){
										
										echo '<option value="'.$year.'">'.$year.'</option>';
									}
								?>							
								</select>
						</td>
					</tr>
					<tr><!-- Row 05 -->
						<td>Gender:</td>
						<td><input name="stu_gender" type="radio" value="M">Male <input name="gender" type="radio" value="F">Female
						</td>
					</tr>
					<tr><!-- Row 06 -->
						<td>Mobile Number:</td>
						<td><input name="stu_mobile" type="text" placeholder="Write your Mobile Number"></td>
					</tr>
					<tr><!-- Row 07 -->
						<td>Email</td>
						<td><input name="email" type="email" placeholder="Write your Email"></td>
					</tr>
					<tr><!-- Row 08 -->
						<td>National ID</td>
						<td><input name="stu_nid" type="text" placeholder="Write your National ID Number"></td>
					</tr>
					<tr><!-- Row 09 -->
						<td>Marital Status:</td>
						<td><input name="stu_mstatus" type="radio" Value="Mr">Married<input name="mstatus" type="radio" Value="Umr">Unmarried</td>
					</tr>
					<tr><!-- Row 10 -->
						<td>Blood Group</td>
						<td>
							<select name="b_group">
							
							<option value="">Blood Group</option>
									<?php
										$blood=array('A+'=>'A<sup>+</sup>','A-'=>'A<sup>-</sup>','B+'=>'B<sup>+</sup>','B-'=>'B<sup>-</sup>','O+'=>'O<sup>+</sup>','O-'=>'O<sup>-</sup>','AB+'=>'AB<sup>+</sup>','AB-'=>'AB<sup>-</sup>');

										foreach($blood as $bvalue=>$bname ){
											echo '<option value="'.$bvalue.'">'.$bname.'</option>';
										}
									
									?>
							</select>
						</td>
					</tr>
					<tr><!-- Row 11 -->
						<td>Division:</td>
						<td>
							<select name="division">
								<option value="">Division</option>
								<option value="dhk">Dhaka</option>
								<option value="ctg">Chittagong</option>
								<option value="raj">Rajshahi</option>
								<option value="syl">Sylhet</option>
								<option value="rang">Rangpur</option>
								<option value="bar">Barisal</option>
								<option value="khu">Khulna</option>
							</select>
						</td>
					</tr>
					<tr><!-- Row 12 -->
						<td>Courses:</td>
						<td><input name="courses1" type="checkbox" value="B">Bangla<br>
							<input name="courses2" type="checkbox" value="E">English<br>
							<input name="courses3" type="checkbox" value="M">Math<br>
							<input name="courses4" type="checkbox" value="I">Islamic Study<br>
							<input name="courses5" type="checkbox" value="C">Cultural<br>
						</td>
					</tr>
					<tr><!-- Row 13 -->
						<td>Applicant FB url:</td>
						<td><input name="fb_url" type="url" placeholder="Write your fb url"></td>
					</tr>
					<tr><!-- Row 14 -->
						<td>Father's Occupation:</td>
						<td><input name="foccu" type="text" placeholder="Write your Father's Occupation"></td>
					</tr>
					<tr><!-- Row 15 -->
						<td>Present Address:</td>
						<td><textarea name="praddr" type="text" placeholder="Write your Present Address"></textarea></td>
					</tr>
					<tr><!-- Row 16 -->
						<td>Permanent Address:</td>
						<td><textarea name="prmaddr" type="text" placeholder="Write your Permanent Address"></textarea></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input name="reset" type="reset" value="Reset">
							<button name="submit" type="submit">Submit</button>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
	</body>

</html>
