<?php include 'reg_validation.php';?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>4th  Class</title>
	   <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	   <link rel="stylesheet" href="assets/css/font-awesome.min.css">
	   <link rel="stylesheet" href="assets/css/style.css">
	  
	   <script src="assets/js/jquery-1.12.4.js"></script>
		
</head>
<body>
		<div class="container">
            <form action="reg_validation.php" method="POST" class="form-horizontal" role="form">
                <h2>Registration Form</h2>

                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_name"  placeholder="Write your Full Name" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="fathername" class="col-sm-3 control-label">Father name</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_fathername"  placeholder="Write your father  Name" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="mothername" class="col-sm-3 control-label">Mother Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_mothername"  placeholder="Write your Mother Name" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" name="email" placeholder="write your Email" class="form-control">
                    </div>
                </div>
               
                <div class="form-group">
                    <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
                    <div class="col-sm-9">
                        <input type="date" name="stu_dob" id="birthDate" class="form-control">
                    </div>
                </div>
               
                <div class="form-group">
                    <label class="control-label col-sm-3">Gender</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="stu_gender" id="femaleRadio" value="Female">Female
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="stu_gender" id="maleRadio" value="Male">Male
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="stu_gender" id="uncknownRadio" value="Unknown">Unknown
                                </label>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <label for="mobile" class="col-sm-3 control-label">Contact Number</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_mobile"  placeholder="Write your Contact Number" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="nid" class="col-sm-3 control-label">National ID</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_nid"  placeholder="Write your National ID" class="form-control" autofocus>
               
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-sm-3">Married Status</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="stu_mstatus" value="Female">Married
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="stu_mstatus" value="Male">Unmarried
                                </label>
                            </div>
                        </div>
                    </div>
                </div> 
                 <div class="form-group">
                    <label for="blood_gr" class="col-sm-3 control-label">Blood Group</label>
                    <div class="col-sm-9">
                        <select name="stu_bloodg" class="form-control">
                        	<option>O+</option>
                            <option>O-</option>
                            <option>A+</option>
                            <option>A-</option>
                            <option>AB+</option>
                            <option>AB-</option>
                            <option>B+</option>
                            <option>B-</option>
                        </select>
                    </div>
                </div> <!-- Blood-group -->
                <div class="form-group">
				 <label for="dev" class="col-sm-3 control-label">Devision</label>
                    <div class="col-sm-9">
                        <select name="stu_dev" class="form-control">
                        	<option>Dhaka</option>
                            <option>Rangpur</option>
                            <option>Chittagong</option>
                            <option>Sylhet</option>
                            <option>Borisal</option>
                            <option>Khulna</option>
                            <option>Rajshahi</option>
                            <option>Moymonshing</option>
                        </select>
                    </div>
                </div> <!-- Blood-group -->
                <div class="form-group">
                    <label class="control-label col-sm-3">Course name</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
						   	<label class="checkbox-inline">
						      <input type="checkbox" name="stu_course" value="">CSE
						    </label>
						    <label class="checkbox-inline">
						      <input type="checkbox" name="stu_course" value="">EEE
						    </label>
						    <label class="checkbox-inline">
						      <input type="checkbox" name="stu_course" value="">IT
						    </label>
						    <label class="checkbox-inline">
						      <input type="checkbox" name="stu_course" value="">ENGLISH
						    </label>
					  </div>
                    </div>
                </div> <!-- /.form-group -->
                 <div class="form-group">
                    <label for="fb_url" class="col-sm-3 control-label">Fb url</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_fburl"  placeholder="Write your Fb url" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="fatheropp" class="col-sm-3 control-label">Father Occupation</label>
                    <div class="col-sm-9">
                        <input type="text" name="stu_fatheropp"  placeholder="Write your Father Occupation" class="form-control" autofocus>
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="pre_add" class="col-sm-3 control-label">Present Address</label>
                    <div class="col-sm-9">
                        <textarea name="stu_padd"  placeholder="Write your Present Address" class="form-control" autofocus></textarea> 
               
                    </div>
                </div>
                <div class="form-group">
                    <label for="per_add" class="col-sm-3 control-label">Permanent Address</label>
                    <div class="col-sm-9">
                        <textarea name="stu_prmaddr"  placeholder="Write your Permanent Address" class="form-control" autofocus></textarea> 
               
                    </div>
                </div>
               <div class="form-group">
				  <label class="col-md-4 control-label"></label>
				  <div class="col-md-4">
				      <button type="submit" name="submit" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
				    </div>
				    
				  <div class="col-md-4">
				     <button type="reset" name="reset" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspRESET<span class="glyphicon glyphicon-repeat"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
				  </div>
				  </div>
				</div>
				
            </form>
        </div> <!-- ./container -->
	

	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/main.js"></script>

</body>
</html>