<?php
	include('db_connection.php');
	


	##echo $_SERVER['SERVER_SOFTWARE'];
	if(isset($_POST['submit'])){
		
		$a_fullname=$_POST['fullname'];
		$a_fname=$_POST['fathername'];
		$a_mname=$_POST['mothername'];
		$a_bday=$_POST['b_day'];
		$a_bmth=$_POST['b_mth'];
		$a_byr=$_POST['b_yr'];
		$a_gender=$_POST['gender'];
		$a_mnumber=$_POST['mnumber'];
		$a_email=$_POST['email'];
		$a_nid=$_POST['nid'];
		$a_mstatus=$_POST['mstatus'];
		$a_bgroup=$_POST['b_group'];
		$a_div=$_POST['division'];
		if(isset($_POST['courses1'])){
			$a_cour1=htmlspecialchars($_POST['courses1']);
		}
		if(isset($_POST['courses2'])){
			$a_cour2=htmlspecialchars($_POST['courses2']);
		}
		if(isset($_POST['courses3'])){
			$a_cour3=htmlspecialchars($_POST['courses3']);
		}
		if(isset($_POST['courses4'])){
			$a_cour4=htmlspecialchars($_POST['courses4']);
		}
		if(isset($_POST['courses5'])){
			$a_cour5=htmlspecialchars($_POST['courses5']);
		}
		$a_fburl=$_POST['fb_url'];
		$a_foccu=$_POST['foccu'];
		$a_praddr=$_POST['praddr'];
		$a_prmaddr=$_POST['prmaddr'];
		
		$error='';
		########### Validation all field ############
		
		######## Applicant Full Name ############
		
		if(strlen($a_fullname)>=6 && strlen($a_fullname)<=50 && str_word_count($a_fullname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fullname)){
			echo $a_fullname.'<br>';
		}
		else{
			$error.= 'Full name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Father's Name ############
		
		if(strlen($a_fname)>=6 && strlen($a_fname)<=50 && str_word_count($a_fname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_fname)){
			echo $a_fname.'<br>';
		}
		else{
			$error.= 'Father name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		######## Applicant Mother's Name ############
		
		if(strlen($a_mname)>=6 && strlen($a_mname)<=50 && str_word_count($a_mname)>=2 && preg_match('/^[-a-zA-Z. ]*$/',$a_mname)){
			echo $a_mname.'<br>';
		}
		else{
			$error.= 'Mother name must be with in 6-50 letters and 2 words minimum..<br>';
		}
		
		
		######## Applicant's Date of Birth ############
		if($a_bday>0 && $a_bday<=31 && $a_bmth >0 && $a_bmth<=12 && $a_byr>=1980 && $a_byr<=2018 ){
			echo $a_bday.'-'.$a_bmth.'-'.$a_byr.'<br>';
		}
		else{
			$error.= 'Your Birthdate is not Correct. <br>';
		}
		
		######## Applicant's Gender ############
		if($a_gender ==='M' || $a_gender ==='F' ){
			echo $a_gender.'<br>';
		}else{
			$error.= 'Gender does not matched. <br>';
		}
		
		######## Applicant's Phone Number ############
		if(strlen($a_mnumber)>=11 && strlen($a_mnumber)<=17 && preg_match('/^[-0-9.+ ]*$/',$a_mnumber)){
			echo $a_mnumber.'<br>';
		}else{
			$error.= 'Phone Number will be like this +8801683432790 <br>';
		}
		
		######## Applicant Email ############
		if(filter_var($a_email, FILTER_VALIDATE_EMAIL)){
			echo $a_email.'<br>';
		}
		else{
			$error.= 'Invalid Email..<br>';
		}
		
		######## Applicant's NID Number ############
		if(strlen($a_nid)>=10 && strlen($a_nid)<=22 && preg_match('/^[0-9]*$/',$a_nid)){
			echo $a_nid.'<br>';
		}else{
			$error.= 'NID Number will be like this 2345671234 <br>';
		}
		
		######## Applicant's Marital Status ############
		if($a_mstatus ==='Mr' || $a_mstatus ==='Umr' ){
			echo $a_mstatus.'<br>';
		}else{
			$error.= 'Marital Status does not matched <br>';
		}
		
		######## Applicant's Blood Group ############
		if($a_bgroup ==='A+' || $a_bgroup ==='A-' || $a_bgroup ==='B+' || $a_bgroup ==='B-' || $a_bgroup ==='O+' || $a_bgroup ==='O-' || $a_bgroup ==='AB+' || $a_bgroup ==='AB-' ){
			echo $a_bgroup.'<br>';
		}else{
			$error.= 'Blood Group does not matched <br>';
		}
		
		######## Applicant's Division Area ############
		if($a_div ==='dhk' || $a_div ==='ctg' || $a_div ==='raj' || $a_div ==='syl' || $a_div ==='rang' || $a_div ==='bar' || $a_div ==='khu'){
			echo $a_div.'<br>';
		}else{
			$error.= 'Division Value does not matched <br>';
		}
		
		
		######## Applicant's Division Area ############
		if(isset($a_cour1)){
			if($a_cour1==='B'){
			echo $a_cour1;
			}else{ 
				$error.= 'Unknown Bangla Course Value <br>';
			}
		}
		if(isset($a_cour2)){
			if($a_cour2==='E'){
				echo $a_cour2;
			}else{ 
				$error.= 'Unknown English Course Value <br>';
			}
		}
		if(isset($a_cour3)){
			if($a_cour3==='M'){
				echo $a_cour3;
			}else{ 
				$error.= 'Unknown Math Course Value <br>';
			}
		}
		if(isset($a_cour4)){
			if($a_cour4==='I'){
				echo $a_cour4;
			}else{ 
				$error.= 'Unknown Islamic Course Value <br>';
			}
		}
		if(isset($a_cour5)){
			if($a_cour5==='C'){
				echo $a_cour5;
			}else{ 
				$error.= 'Unknown Cultural Course Value <br>';
			}
		}
		
		
		########### Applicant Fb Url ############
		if(filter_var($a_fburl, FILTER_VALIDATE_URL)){
			echo $a_fburl.'<br>';
		}
		else{
			$error.= 'Invalid FB Url..<br>';
		}
		
		########### Applicant Father's Occupation ############
		if(strlen($a_foccu)>=4 && strlen($a_foccu)<=20 && str_word_count($a_foccu)>=1 && preg_match('/^[-a-zA-Z. ]*$/',$a_foccu)){
			echo $a_foccu.'<br>';
		}
		else{
			$error.= 'Father Occupation must be with in 4-20 letters..<br>';
		}
		
		########### Applicant Present Address ############
		if(strlen($a_praddr)>=4 && strlen($a_praddr)<=100 && str_word_count($a_praddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_praddr)){
			echo $a_praddr.'<br>';
		}
		else{
			$error.= 'Address must be with in 4-100 letters..<br>';
		}
		
		########### Applicant Parmanent Address ############
		if(strlen($a_prmaddr)>=4 && strlen($a_prmaddr)<=100 && str_word_count($a_prmaddr)>=1 && preg_match('/^[-a-zA-Z0-9 ]/',$a_prmaddr)){
			echo $a_prmaddr.'<br>';
		}
		else{
			$error.= 'Parmanent Address must be with in 4-100 letters..<br>';
		}
		
		
		
		############### Inserting Process ############
		
		if(!$error){
			
			//// date of birth
			$a_dob= $a_bday.'-'.$a_bmth.'-'.$a_byr;
			///// Selected Courses
			$course=$a_cour1.'-'.$a_cour2.'-'.$a_cour3.'-'.$a_cour4.'-'.$a_cour5;
			
			$connt=db_connect();
			
			$sql_insert="INSERT INTO student_registration(student_name, stu_father_name, stu_mother_name, stu_dob, stu_gender, stu_phone, stu_email, stu_nid, stu_mstatus, stu_bgroup, stu_div, stu_course, stu_fburl, stu_foccu, stu_paddr, stu_prmaddr) VALUES ('$a_fullname','$a_fname','$a_mname','$a_dob','$a_gender','$a_mnumber','$a_email','$a_nid','$a_mstatus', '$a_bgroup','$a_div','$course','$a_fburl','$a_foccu','$a_praddr','$a_prmaddr' )";
			
			$result=mysqli_query($connt,$sql_insert);
			
			if(!$result){
				die ('Error: '.mysqli_error($connt));
			}else{
				echo 'Data Insert Successfully';
			}
		
		}else{
			echo 'Error: <br>'.$error;
		}
		
		########### echo all field ############
		
		
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';
	}
	
	
?>
