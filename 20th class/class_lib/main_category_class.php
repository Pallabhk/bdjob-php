<?php
	
	require_once('db_connection_class.php');
	
	class Main_Category extends DB_CONNECT{
		
		public function main_categ_insert($data){
			
			#### Directory Path ##################
			$directory_root='../images/all_products/';
			
			########## Main Category Data ##########
			$Main_category_name=$data['main_categ_name'];
			$category_folder_name=strtolower($data['main_categ_name']);
			
			if(strlen($category_folder_name)>=3 && strlen($category_folder_name)<=20 && preg_match('/^[-a-zA-Z ]*$/',$category_folder_name)){
				
				if(str_word_count($category_folder_name)>1){
					###### for multiple word ##############
					$foldername=explode(' ',$category_folder_name);
					//print_r($foldername);
					//echo '<br>';
					
					//$new_foldername=implode('_',$foldername);
					//print_r($new_foldername);
					
					
					$category_folder_name=implode('_',$foldername);
					
				}
				if(!file_exists($directory_root.$category_folder_name)){
					################## DB Insert #############
					
					$db_connt=$this->connect;
					$sql_insert="INSERT INTO main_category (main_categ_name, main_categ_folder) VALUES ('$Main_category_name', '$category_folder_name')";
					
					$db_connt->query($sql_insert);
					if($db_connt->error){
						echo '<div class="alert alert-warning text-center" role="alert">Error: '.$db_connt->error .'</div>';
					}else{
						echo '<div class="alert alert-success text-center" role="alert">Main Category Upload Successfully</div>';
						######### make Directory/Folder ########
						mkdir($directory_root.$category_folder_name,0770);
						
						header('refresh:1; url=add_category.php');
					}
					
				}else{
					
					echo '<div class="alert alert-warning text-center" role="alert">We Already have this folder</div>';
				}
			
		}
		else{
			
			echo '<div class="alert alert-warning text-center" role="alert">Main Category will be in 3-20 letters</div>';
		}
			
			
		}// main_categ_insert method
		
		#########################################
		########## Main Category View ###########
		public function main_categ_view(){
			
			$db_connt=$this->connect;
			$sql_view="SELECT * FROM main_category";
			$result=$db_connt->query($sql_view);
			
			if($db_connt->error){
				die('Error: '.$db_connt->error);
			}else{
				return $result;
			}
		}// main_categ_view method
		
	}// Main_Category class
	
	
	
	
	
	
	
	
	
	
	
	
	
?>