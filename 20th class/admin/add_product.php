<?php 
ob_start();
include('header.php');
if(!$_SESSION['action']){
	header('Location: index.php');
}
	require_once('../class_lib/add_product_class.php');
	$last_id_obj= new Add_Product;
	$last_id_obj->last_id();

if (isset($_POST['prod_submit'])) {
	
	echo '<pre>';
	print_r($_POST);
	$prod_categ=explode('#',$_POST['prod_categ']);
	print_r($prod_categ);
	$main_categ_name=$prod_categ[0];
	$sub_categ_name=$prod_categ[1];

	echo $prod_code =strtoupper(substr($main_categ_name,0,1)).$_POST['prod_code']);
	$prod_name      =$_POST['prod_name'];
	$prod_price     =$_POST['prod_price'];
	$prod_desc      =$_POST['prod_desc'];
	
	print_r($_FILES);
	echo '</pre>';

	###Varialble name#######

	$imagename    =$_FILES['prod_img']['name'];
	$imagetemname =$_FILES['prod_img']['tmp_name'];
	$imagesize    =$_FILES['prod_img']['size'];


####image Check Function######
		

	//echo pathinfo($imagename,PATHINFO_EXTENTION);
	//$target_extention =array('pdf','docx');

	###Other file Uploading####

	$img_check =getimagesize($imagetemname);

	if ($img_check==true) {
		//image size validation
		if ($imagesize >150000) {
		
			echo "<div class='alert alert-warning'>File size will be not more than 100kb..</div>";

		}else{

	move_uploaded_file($imagetemname,"../images/all_products/".$main_categ_name."/".$sub_categ_name."/".time().$imagename);
		}
	}else{

		echo "<div class='alert alert-warning'>We need only jpg and png file..</div>";
	}
}
?>

		<!-- ===============###########=== Content Part Start ======################=============== -->	
			<div class="col-sm-8">

				<!-- ======================== Add Product =============== -->
				<h3 class="alert alert-success text-center">Add Product</h3>
<?php
	require_once('../class_lib/main_category_class.php');
	$main_categ_obj= new Main_Category;
	/////////// for Sub Category form
	$main_categ_data=$main_categ_obj->main_categ_view();
?>
				<form method="post" enctype="multipart/form-data" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
				  <div class="form-group">
					<label for="prod_categ">Choose Product Category</label>
					<select name="prod_categ" class="form-control" id="prod_categ">
						<option value="">Choose Product Category</option>
				<?php
					if($main_categ_data->num_rows>0){
						while($main_categ_list=$main_categ_data->fetch_assoc()){
							
							$main_categ_name=$main_categ_list['main_categ_name'];
							$main_categ_value=$main_categ_list['main_categ_folder'];
							
							echo '<option disabled="disabled" style="font-weight:700; background-color:skyblue; border-bottom:1px solid black;">'.$main_categ_name.'</option>';
							
							########################## Sub Category view	
								require_once('../class_lib/sub_category_class.php');
								$sub_categ_obj= new Sub_Category;
								$sub_category_table=$sub_categ_obj->sub_categ_view_main($main_categ_value);
								
								if($sub_category_table->num_rows > 0){
									while($sub_categ_list=$sub_category_table->fetch_assoc()){
										$sub_categ_name=$sub_categ_list['sub_categ_name'];
										$sub_categ_value=$sub_categ_list['sub_categ_folder'];
										///print_r($sub_categ_list);
										
										echo '<option value="'.$main_categ_value.'#'.$sub_categ_value.'">'.$sub_categ_name.' of '.$main_categ_name.'</option>';
										
									}////// While Loop							
								}
								
						}
					}else{
						echo '<option value="">There have no Main Category</option>';
					}
				
				?>
					</select>
				  </div>
				  <div class="form-group">
					<label for="prod_code">Product Code</label>
					<input name="prod_code" type="text" class="form-control" id="prod_code"  placeholder="Product Code">
				  </div>
				  <div class="form-group">
					<label for="prod_name">Product Name</label>
					<input name="prod_name" type="text" class="form-control" id="prod_name"  placeholder="Product Name">
				  </div>
				  <div class="form-group">
					<label for="prod_price">Product Price</label>
					<input name="prod_price" type="text" class="form-control" id="prod_price"  placeholder="Product Price">
				  </div>
				  <div class="form-group">
					<label for="prod_desc">Product Description</label>
					<textarea name="prod_desc" type="text" rows="3" class="form-control" id="prod_desc"  placeholder="Product Description"></textarea>
				  </div>
				  <div class="form-group">
					<label for="prod_img">Product Image</label>
					<input type="file" name="prod_img" id="prod_img">
				  </div>
				  <div class="form-group">
				  <button name="prod_submit" type="submit" class="btn btn-primary">Product Submit</button>
				  </div>
				</form>
				<br>
				
				
			</div><!-- Content div -->
		
		<!-- ===============###########=== Content Part close ======################=============== -->	


<script>
	$(document).ready(function()){
		$('#prod_categ').click(function)
	}
</script>		
<?php 
include('footer.php'); 
ob_end_flush();
ob_end_clean();
?>
