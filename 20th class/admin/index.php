<?php
	session_start();
	########### Session Check ############
	if(isset($_SESSION['admin_name'])){
		header('Location: admin.php');
	}else if(isset($_SESSION['r_admin_name'])){
		header('Location: r_admin.php');
	}
	
	
	########## User Login ################
	if(isset($_POST['a_submit'])){
		if(!empty($_POST['a_email']) && !empty($_POST['a_pass'])){
			require_once('../class_lib/admin_access_class.php');
			$admin_login_obj = new Admin_Access;
			$admin_login_obj->admin_login($_POST);
		}else{
			echo '<div class="alert alert-warning text-center" role="alert">Please Enter Email and Password..</div>';
		}
	}

?>


<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container"> 

	<form method="post" class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
	
	  <h1 class="text-center">Admin Login</h1>
	  <div class="form-group">
		<label for="email">Email address</label>
		<input name="a_email" type="email" class="form-control" id="email" value="<?php if(isset($_COOKIE['user_email'])){ echo $_COOKIE['user_email']; }?>" placeholder="Email">
	  </div>
	  <div class="form-group">
		<label for="password">Password</label>
		<input name="a_pass" type="password" class="form-control" id="password" value="<?php if(isset($_COOKIE['user_pass'])){ echo $_COOKIE['user_pass']; }?>" placeholder="Password">
	  </div>
	  <div class="checkbox">
		<label>
		  <input name="a_remb" type="checkbox" value="Y" <?php if(isset($_COOKIE['user_email'])){ echo 'checked'; }?> > Remember Me
		</label>
	  </div>
	  <button name="a_submit" type="submit" class="btn btn-default">Submit</button>
	</form>

</div> 

</body>
</html>
