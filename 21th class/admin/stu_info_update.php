<?php
	if(isset($_REQUEST['stu_edit'])){
		require_once('../functions.php');
		$stu_info=student_data_info($_REQUEST['stu_edit']);
		$stu_data=mysqli_fetch_assoc($stu_info);
		$dob=explode('-',$stu_data['stu_dob']);
		//print_r($dob=explode('-',$stu_data['stu_dob']));
	}
	if(isset($_POST['u_submit'])){
		require_once('../functions.php');
		student_info_update($_POST);
	}
	
?>
<section style="padding:20px 10px;">
		<form action="" method="post" style="width:600px; margin:auto;">
			<fieldset>
				<legend><mark>Registration Form</mark></legend>
				<table>
					<input type="hidden" name="sl_id" value="<?php echo $stu_data['sl_id']; ?>">
					<tr><!-- Row 01 -->
						<td>Name:</td>
						<td><input name="fullname" type="text" value="<?php echo $stu_data['student_name']; ?>"  placeholder="Write your Full Name"></td>
					</tr>
					<tr><!-- Row 02 -->
						<td>Father's Name:</td>
						<td><input name="fathername" type="text" value="<?php echo $stu_data['stu_father_name']; ?>" placeholder="Write your Father Name"></td>
					</tr>
					<tr><!-- Row 03 -->
						<td>Mother's Name:</td>
						<td><input name="mothername" type="text" value="<?php echo $stu_data['stu_mother_name']; ?>" placeholder="Write your Mother Name"></td>
					</tr>
					<tr><!-- Row 04 -->
						<td>Date of Birth</td>
						<td>Day:<select name="b_day">
								<option value="">Day</option>
									<?php
										for($day=1; $day<=31;$day++){
											if($day==$dob[0]){
											echo '<option value="'.$day.'" selected>'.$day.'</option>';
											}else{
											echo '<option value="'.$day.'">'.$day.'</option>';
											}
										}
									?>
							</select>
							Month:
							<select name="b_mth">
								<option value="">month</option>
									<?php
										$month=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');

										foreach($month as $mvalue=>$mname ){
											if($mvalue==$dob[1]){
											echo '<option value="'.++$mvalue.'" selected>'.$mname.'</option>';
											}else{
											echo '<option value="'.++$mvalue.'">'.$mname.'</option>';
											}
										}
									
									?>
							</select>
							Year:
							<select name="b_yr">
								<option value="">year</option>
								<?php
									for($year=2018; $year>=1980;$year--){
										if($year==$dob[2]){
										echo '<option value="'.$year.'" selected>'.$year.'</option>';
										}else{
										echo '<option value="'.$year.'">'.$year.'</option>';
										}
									}
								?>							
								</select>
						</td>
					</tr>
					<tr><!-- Row 05 -->
						<td>Gender:</td>
						<td><input name="gender" type="radio" value="M" <?php if($stu_data['stu_gender']=='M'){ echo 'checked';} ?>>Male <input name="gender" type="radio" value="F" <?php if($stu_data['stu_gender']=='F'){ echo 'checked';} ?>>Female
						</td>
					</tr>
					<tr><!-- Row 06 -->
						<td>Mobile Number:</td>
						<td><input name="mnumber" type="text" value="<?php echo $stu_data['stu_phone']; ?>" placeholder="Write your Mobile Number"></td>
					</tr>
					<tr><!-- Row 07 -->
						<td>Email</td>
						<td><input name="email" type="email" placeholder="<?php echo $stu_data['stu_email']; ?>"></td>
					</tr>
					<tr><!-- Row 08 -->
						<td>National ID</td>
						<td><input name="nid" type="text" value="<?php echo $stu_data['stu_nid']; ?>" placeholder="Write your National ID Number"></td>
					</tr>
					<tr><!-- Row 09 -->
						<td>Marital Status:</td>
						<td><input name="mstatus" type="radio" Value="Mr" <?php if($stu_data['stu_mstatus']=='Mr'){ echo 'checked';} ?>>Married<input name="mstatus" type="radio" Value="Umr" <?php if($stu_data['stu_mstatus']=='Umr'){ echo 'checked';} ?>>Unmarried</td>
					</tr>
					<tr><!-- Row 10 -->
						<td>Blood Group</td>
						<td>
							<select name="b_group">
							
							<option value="">Blood Group</option>
									<?php
										$blood=array('A+'=>'A<sup>+</sup>','A-'=>'A<sup>-</sup>','B+'=>'B<sup>+</sup>','B-'=>'B<sup>-</sup>','O+'=>'O<sup>+</sup>','O-'=>'O<sup>-</sup>','AB+'=>'AB<sup>+</sup>','AB-'=>'AB<sup>-</sup>');

										foreach($blood as $bvalue=>$bname ){
											if($bvalue==$stu_data['stu_bgroup']){
											echo '<option value="'.$bvalue.'" selected>'.$bname.'</option>';
											}else{
											echo '<option value="'.$bvalue.'">'.$bname.'</option>';
											}
										}
									
									?>
							</select>
						</td>
					</tr>
					<tr><!-- Row 11 -->
						<td>Division:</td>
						<td>
							<select name="division">
								<option value="">Division</option>
							<?php
										$division=array('dhk'=>'Dhaka','ctg'=>'Chittagong','raj'=>'Rajshahi','syl'=>'Sylhet','rang'=>'Rangpur','bar'=>'Barisal','khu'=>'Khulna');

										foreach($division as $dvalue=>$dname ){
											if($dvalue==$stu_data['stu_div']){
											echo '<option value="'.$dvalue.'" selected>'.$dname.'</option>';
											}else{
											echo '<option value="'.$dvalue.'">'.$dname.'</option>';
											}
										}
									
									?>
							</select>
						</td>
					</tr>
					<tr><!-- Row 12 -->
						<td>Courses:</td>
						<td><input name="courses1" type="checkbox" value="B">Bangla<br>
							<input name="courses2" type="checkbox" value="E">English<br>
							<input name="courses3" type="checkbox" value="M">Math<br>
							<input name="courses4" type="checkbox" value="I">Islamic Study<br>
							<input name="courses5" type="checkbox" value="C">Cultural<br>
						</td>
					</tr>
					<tr><!-- Row 13 -->
						<td>Applicant FB url:</td>
						<td><input name="fb_url" type="url" value="<?php echo $stu_data['stu_fburl']; ?>" placeholder="Write your fb url"></td>
					</tr>
					<tr><!-- Row 14 -->
						<td>Father's Occupation:</td>
						<td><input name="foccu" type="text"  value="<?php echo $stu_data['stu_foccu']; ?>" placeholder="Write your Father's Occupation"></td>
					</tr>
					<tr><!-- Row 15 -->
						<td>Present Address:</td>
						<td><textarea name="praddr" type="text"  placeholder="Write your Present Address"><?php if(isset($stu_data['stu_paddr']))echo $stu_data['stu_paddr']; ?></textarea></td>
					</tr>
					<tr><!-- Row 16 -->
						<td>Permanent Address:</td>
						<td><textarea name="prmaddr" type="text" placeholder="Write your Permanent Address"><?php if(isset($stu_data['stu_prmaddr']))echo $stu_data['stu_paddr']; ?></textarea></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input name="reset" type="reset" value="Reset">
							<button name="u_submit" type="submit">Submit</button>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
</section>
