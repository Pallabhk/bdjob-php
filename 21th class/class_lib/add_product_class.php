<?php
	
	require_once('db_connection_class.php');
	
	class Add_Product extends DB_CONNECT{
		
		public function product_insert($data){
				////echo '<pre>';
				////print_r($_POST);
				###### Product info Variables###############
				$prod_categ=explode('#',$_POST['prod_categ']); // product category separation
				///print_r($prod_categ);
				$main_categ_name=$prod_categ[0];
				$sub_categ_name=$prod_categ[1];
				
				$prod_code=strtoupper(substr($main_categ_name,0,1)).$_POST['prod_code'];
				$prod_name=$_POST['prod_name'];
				$prod_price=$_POST['prod_price'];
				$prod_desc=$_POST['prod_desc'];
				
				
				////print_r($_FILES);
				////echo '</pre>';
				
				//////////// image files variables #############
				$imagename=$_FILES['prod_img']['name'];
				$imageTempname=$_FILES['prod_img']['tmp_name'];
				$imagesize=$_FILES['prod_img']['size'];
				
				$image_url="images/all_products/".$main_categ_name."/".$sub_categ_name."/".$prod_code.time().$imagename;
				############# image check function #############
				//// ********** Other files Uploading ***********
				/*
				echo pathinfo($imagename,PATHINFO_EXTENSION);
				$my_extenstion= pathinfo($imagename,PATHINFO_EXTENSION);
				
				$target_extenstion= array('pdf','docx');
				$img_check=in_array($my_extenstion,$target_extenstion);
				*/
				//// ********** Other files Uploading ***********
				
				$img_check=getimagesize($imageTempname);
				if($img_check==true){
					//// image size validation
					if($imagesize > 120000){
						echo '<div class="alert alert-warning">File size will be not more then 100kb</div>';
					}else{
					################## DB Insert #############
					
					$db_connt=$this->connect;
					$sql_insert="INSERT INTO all_products (product_code, product_name, product_price, product_desc, product_img, product_main_categ, product_sub_categ) VALUES ('$prod_code', '$prod_name','$prod_price','$prod_desc', '$image_url','$main_categ_name','$sub_categ_name')";
					
					$db_connt->query($sql_insert);
						if($db_connt->error){
							echo '<div class="alert alert-warning text-center" role="alert">Error: '.$db_connt->error .'</div>';
						}else{
							echo '<div class="alert alert-success text-center" role="alert">Product Upload Successfully</div>';
							
							move_uploaded_file($imageTempname,"../".$image_url);
							
							header('refresh:1; url=add_product.php');
						}
						
						
					}
				}else{
					echo '<div class="alert alert-warning">We need only jpg, jpeg or png image file..</div>';
				}

		
		
		}// product_insert method
		
		#############################################
		///// Last ID number of Product table ///////
		public function last_id(){
			$db_connt=$this->connect;
			$sql_last_id="SELECT sl_id FROM all_products ORDER BY sl_id DESC LIMIT 0,1 ";
			$result=$db_connt->query($sql_last_id);
			if($db_connt->error){
					die('Error: '.$db_connt->error);
			}else{
				$last_id=$result->fetch_assoc();
				return $last_id['sl_id']+1;
			}
			
		}//last_id method
		
	}// Add_Product class
	
	
	
	
	
	
	
	
	
	
	
	
	
?>