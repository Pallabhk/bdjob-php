<?php
	include('header.php');
	//require('header.php');

?>




<!-- ============= Body Content Part ================ -->	
	<div class="container-fluid">
	  <div id="main_slider" class="carousel slide row" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
		  <li data-target="#main_slider" data-slide-to="0" class="active"></li>
		  <li data-target="#main_slider" data-slide-to="1"></li>
		  <li data-target="#main_slider" data-slide-to="2"></li>
		  <li data-target="#main_slider" data-slide-to="3"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">

		  <div class="item active">
			<img src="images/slider/bg-1.jpeg" alt="Chania">
			<div class="carousel-caption">
			  <h3>Chania</h3>
			  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
			</div>
		  </div>

		  <div class="item">
			<img src="images/slider/bg-1.jpeg" class="image-responsive" alt="Chania">
			<div class="carousel-caption">
			  <h3>Chania</h3>
			  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
			</div>
		  </div>
		
		  <div class="item">
			<img src="images/slider/bg-1.jpeg" class="image-responsive" alt="Flower">
			<div class="carousel-caption">
			  <h3>Flowers</h3>
			  <p>Beautiful flowers in Kolymbari, Crete.</p>
			</div>
		  </div>

		  <div class="item">
			<img src="images/slider/bg-1.jpeg" class="image-responsive" alt="Flower">
			<div class="carousel-caption">
			  <h3>Flowers</h3>
			  <p>Beautiful flowers in Kolymbari, Crete.</p>
			</div>
		  </div>
	  
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#main_slider" role="button" data-slide="prev">
		  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		  <span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#main_slider" role="button" data-slide="next">
		  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		  <span class="sr-only">Next</span>
		</a>
	  </div>
	</div>

	
<section id="gallery">
	<div class="container">
		<a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
      <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-4.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-4.jpg" alt="" /></a>
      <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-5.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-5.jpg" alt="" /></a>
      <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-6.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-6.jpg" alt="" /></a>	
	  
	</div>
</section>   


<!-- ============ Footer Part ============= -->
<?php
	require'footer.php';
?>