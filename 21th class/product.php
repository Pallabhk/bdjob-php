<?php 

include'header.php';
include('product_nav.php');
 ?>

<style>
#all_product p{
	border-top: 1px solid gray;
	padding-top:10px;
	font-size: 1.2em; 
}
#all_product a{
	text-decoration: none;
}
</style>
<!--Body Content-->

	<section id="all_product">
		<div class="container">
			<div class="row">
				<?php
					require_once('class_lib/userproductview_class.php');

					$product_obj = new User_productview();
					$product = $product_obj->all_product_view();

					if ($product->num_rows>0) {

						while($pro_data=$product->fetch_assoc()){

							// print_r($pro_data);

							$product_img  =$pro_data['product_img'];
							$product_name =$pro_data['product_name'];
							$product_price=$pro_data['product_price'];
							$product_code =$pro_data['product_code'];

							?>
						<div class="col-sm-3 col-xs-6">
							<a href="product_code.php" class="thumbnail">
								<img src="<?php echo $product_img;?>" alt="">
								<div class="caption text-center">
									<h3><?php echo $product_name;?></h3>
									<p><?php echo $product_price;?></p>
									<p><?php echo $product_code;?> </p>
								</div>
							</a>
						</div>
					<?php } } ?>
				
			</div>
		</div>
	</section>
<?php include'header.php';?>