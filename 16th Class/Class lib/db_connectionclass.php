<?php
	
	require_once('../db_details.php');

	class Db_connect{
		private $db_host     =DB_HOST;
		private $db_user     =DB_USER;
		private $db_password =DB_PASSWORD;
		private $db_name     =DB_NAME;


		protected $connect;

		public function __construct(){

			$this->db_cont();
		}

		private function db_cont(){

			$this->connect =new mysqli($this->db_host, $this->db_user,$this->db_password , $this->db_name);
			if ($this->connect->connect_error) {
				

				echo die("Error:".$this->connect->connect_error);
			}else{

				return $this->connect;
			}
		}
	}
?>