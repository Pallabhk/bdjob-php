<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Hora krishna Roy">
    <meta name="discription" content="portfolio">
    <meta name="keyword" content="html,css3,Bootstrap">
    <!-- The above 3 meta tags *must*  come first in the head; any other head content must come *after* these tags -->

    <!--Company icon-->
    <link href="img/icon/apple-touch-icon.png" rel=apple-touch-icon>
  	<link href="img/icon/favicon.ico" rel="icon">
    <link rel="icon" href="img/icon/facebook.png" size="32x32">
    <title>Hora Krishna Roy</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<!-- Font-awsome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/lightbox.min.css" rel="stylesheet">


  </head>
  <body>
    

  	<header class="content">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-2 col-sm-2 col-xs-8">
  					<div class="logo">
  						<img class="img-responsive" src="img/favicon.ico" alt="">
  					</div>
  				</div>
  				<div class="col-md-3 col-sm-4 col-xs-8 pull-right">
  					<div class="social-icon">
  						<ul class="list-inline">
  							<li><a href=""><i class="fa fa-facebook"></i></a></li>
  							<li><a href=""><i class="fa fa-twitter"></i></a></li>
  							<li><a href=""><i class="fa fa-youtube"></i></a></li>
  							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
  						</ul>
  					</div>
  				</div>
  				<div class="col-md-7 col-sm-4 col-xs-12 pull-left">
  					<div class="Name">
  						<h2>Hora Krishna Roy</h2>
  					</div>
  				</div>
  				
  			</div>
  		</div>
  	</header>
  	
     <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">WebSiteName</a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
              <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">About us<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">About us</a></li>
                  <li><a href="#">Service</a></li>
                  <li><a href="#">Product</a></li>
                </ul>
              </li>
            <li><a href="#">Web design</a></li>
            <li><a href="#">PHP development</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        <li><a  type="button" href="registrationpage.php" data-toggle="modal" data-target=".bs-example-modal-lg><span class="glyphicon glyphicon-user"></span> Registration

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                ...
              </div>
            </div>
          </div>
        </a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>