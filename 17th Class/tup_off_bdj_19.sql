-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2018 at 01:01 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tup_off_bdj_19`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_access`
--

CREATE TABLE `admin_access` (
  `sl_id` int(2) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_pass` varchar(100) NOT NULL,
  `admin_action` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_access`
--

INSERT INTO `admin_access` (`sl_id`, `admin_name`, `admin_email`, `admin_pass`, `admin_action`) VALUES
(1, 'Md. Sharif Ullah Sarkar', 'manikbd.888@gmail.com', 'manikbd123456', 'root_admin'),
(2, 'Manik Sarkar', 'manikbd.fx@gmail.com', '123456', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE `student_registration` (
  `sl_id` int(3) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `stu_father_name` varchar(50) NOT NULL,
  `stu_mother_name` varchar(50) NOT NULL,
  `stu_dob` varchar(12) NOT NULL,
  `stu_gender` varchar(2) NOT NULL,
  `stu_phone` varchar(17) NOT NULL,
  `stu_email` varchar(100) NOT NULL,
  `stu_nid` varchar(24) NOT NULL,
  `stu_mstatus` varchar(3) NOT NULL,
  `stu_bgroup` varchar(3) NOT NULL,
  `stu_div` varchar(5) NOT NULL,
  `stu_course` varchar(2) NOT NULL,
  `stu_fburl` text NOT NULL,
  `stu_foccu` varchar(20) NOT NULL,
  `stu_paddr` text NOT NULL,
  `stu_prmaddr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`sl_id`, `student_name`, `stu_father_name`, `stu_mother_name`, `stu_dob`, `stu_gender`, `stu_phone`, `stu_email`, `stu_nid`, `stu_mstatus`, `stu_bgroup`, `stu_div`, `stu_course`, `stu_fburl`, `stu_foccu`, `stu_paddr`, `stu_prmaddr`) VALUES
(4, 'Manik Sarkar', 'Wali Ullah Sarkar', 'Nazma Sarker', '10-10-2010', 'M', '+8801683432790', 'demo@demo.com', '123456789012', 'Mr', 'B+', 'dhk', '--', 'http://localhost/', 'Retired', '35/3, North Golapbagh', '35/3, North Golapbagh'),
(8, 'Md. Sharif Ullah Sarkar', 'Md. Wali Ullah Sarkar', 'Nazma Sarker', '18-2-2003', 'M', '01683432790', 'youthict00@gmail.com', '123456789012', 'Mr', 'A+', 'dhk', 'B-', 'http://localhost/TUP-OFF-BDJ-19/php_class03-10-01-2018/registrationpage.php', 'Retired', '35/3, North Golapbagh', '35/3, North Golapbagh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_access`
--
ALTER TABLE `admin_access`
  ADD PRIMARY KEY (`sl_id`),
  ADD UNIQUE KEY `admin_email` (`admin_email`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`sl_id`),
  ADD UNIQUE KEY `stu_email` (`stu_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_access`
--
ALTER TABLE `admin_access`
  MODIFY `sl_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `sl_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
