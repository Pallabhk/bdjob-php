<?php
	
	session_start();

	//echo 'Yes you are a Admin and your name is '.$_SESSION['admin_name'];

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  	<header>
  		<nav class="navbar navbar-default">
	  		<div class="container">
			    <div class="col-sm-2 col-xs-2 navbar-header">
			      <a class="navbar-brand" style="padding-top:1px;" href="#">

			        <img alt="Brand" src="../images/logo.PNG" class="img-responsive" style="height: 50px;">
			      </a>
			    </div>
			    <div class="col-sm-8 col-xl-12">
			    	<h3 class="text-center">

			    		<?php 
			    			if (isset($_SESSION['r_admin_name'])) {
			    				
			    			}

			    		?>
			    			
			    	</h3>
			    </div>

			    <button type="button" class="btn btn-info navbar-btn pull-right"data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-sign-out" aria-hidden="true">Log out</i></button>

			</div>
		</nav>
		<!--Log out Model -->

		<button type="button" class="btn btn-primary" ></button>
	
			<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content" style="padding: 25px 10px;">
		      <h4 class="text-center">Do you Want to Logout?</h4>
			<div class="text-center">

				<a role="button" href="?logout=logout" class="btn btn-danger">Yes</a>
				<?php
					if (isset($_REQUEST['logout'])) {
						include'../class_lib/admin_access_class.php';

						$logout_obj = new Admin_Access();
						$logout_obj = new admin_logout();
					}
				?>
		       <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
			</div>
		  </div>
		 </div>
		</div>
		<!--Log out Model -->
  	</header>